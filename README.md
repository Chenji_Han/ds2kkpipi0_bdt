
### environment :

- ROOT6
- python2.7 


### required c++ library :

- xgboost version 0.82
> `git clone --recursive https://github.com/dmlc/xgboost`  
> `cd xgboost`  
> `git checkout release_0.82`  
> `make`  
> `cd rabit; make; cd ..`  
> `cd dmlc-core; make; cd ..`  

### required python2.7 package :

- numpy
- pandas
- xgboost
- sklearn
- PyTables 
- matplotlib
- python-graphviz
- seaborn


### setup 
1. setup the requried environment
2. link the compiled **xgboost** package to the `apply-pkg/` directory
3. `source setup.sh` : compile the applicaiton module in the `apply-pkd` directory


### Work Flow
1. determine the training variables in the *inputs.txt* file
2. convert the *.root* file to *.csv* and *.h5* file in the directory `sample` : `python root2csv.py`
3. implement the primary cutting and generate *train.dat*, *test.dat*, and *val.dat* files in `train/dat`
4. train by running `sh run.sh` in the `train` directory where the hyper-parameters can be changed in `train/run.sh` file
5. to submit the condor jobs, run `sh submit.sh` in the `train/condorJobs` directory where *submit.sh* loops the hyper-parameters









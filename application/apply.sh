#########################################################################
# File Name: apply.sh
# Author: Chenji Han
# mail: hanchenji16@mails.ucas.ac.cn
# Created Time: 2020年07月30日 星期四 14时20分39秒
#########################################################################
#!/bin/bash

EXECUTE='/public/data/hancj/ds2kkpipi0_bdt/apply-pkg/bin/apply'

${EXECUTE} Ds2KKPiMCr
${EXECUTE} Ds2KKPiDatar
#${EXECUTE} Ds2KKPiDatafullca


#!/usr/bin/python

from __future__ import print_function

import matplotlib
matplotlib.use('Agg')

from collections import Counter
import matplotlib.pyplot as plt
import graphviz
import time, os, sys
import pandas as pd
import numpy as np
import xgboost as xgb
import scipy.sparse
from sklearn import metrics
from random import sample
import seaborn as sns

def drawConfusionMatrix(truth, prediction, tag, outputdir) :

    cm = metrics.confusion_matrix(truth, prediction)

    fig = plt.figure()
    ax= plt.subplot()
    sns.heatmap(cm, annot=True, ax = ax, cmap='Greens',fmt='g'); #annot=True to annotate cells
    
    # labels, title and ticks
    ax.set_xlabel('Predicted categories');ax.set_ylabel('True categories'); 
    ax.set_title('confusion matrix of {}'.format(tag)); 
    ax.xaxis.set_ticklabels(['bkg', 'signal']); ax.yaxis.set_ticklabels(['bkg', 'signal']);
    ax.plot()
    fig.savefig(outputdir+'cm_{}.png'.format(tag))

def drawTrainingProcess(results,outputdir) :
    epochs = len(results['train']['auc'])
    x_axis = range(0, epochs)
    # plot log loss
    fig, ax = plt.subplots()
    ax.plot(x_axis, results['train']['auc'], label='training set')
    ax.plot(x_axis, results['eval']['auc'], label='validation set')
    ax.legend()
    plt.ylabel('auc')
    plt.xlabel('rounds')
    plt.title('')
    plt.savefig(outputdir+'trainingProcess.png')


def drawTree(myboost, outputdir) :

    fig, ax = plt.subplots(figsize=(500, 400))
    xgb.plot_tree(myboost, num_trees=2, ax=ax)
    fig = matplotlib.pyplot.gcf()
    fig.set_size_inches(500, 400)
    plt.savefig(outputdir+'tree.pdf')

def drawFeatureImportance(myboost, outputdir) :

    xgb.plot_importance(myboost,importance_type='gain',show_values=False)
    plt.savefig(outputdir+'feature_importance.png',bbox_inches='tight')

def drawRoc(dset1, dset1_predictions, tag1,
            dset2, dset2_predictions, tag2,
            outputdir) :

    fpr1, tpr1, _ = metrics.roc_curve(dset1.get_label(), dset1_predictions, sample_weight=dset1.get_weight())
    roc_auc1 = metrics.auc(fpr1, tpr1)

    fpr2, tpr2, _ = metrics.roc_curve(dset2.get_label(), dset2_predictions, sample_weight=dset2.get_weight())
    roc_auc2 = metrics.auc(fpr2, tpr2)

    plt.figure()

    plt.plot(1-fpr1, tpr1, color='darkorange',
             lw=2, label='ROC curve of {}'.format(tag1) + ' (area = %0.4f)' % roc_auc1)
    plt.plot(1-fpr2, tpr2, color='blue',
             lw=2, label='ROC curve of {}'.format(tag2) + ' (area = %0.4f)' % roc_auc2)

    plt.xlim([-0.02, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('signal reconstruction efficiency')
    plt.ylabel('background rejection ability')
    plt.title('ROC curve')
    plt.legend(loc="lower left")


    plt.savefig(outputdir+'roc_curve.png')


def readFeatures() :
    vbl_names = []
    with open("/public/data/hancj/ds2kkpipi0_bdt/inputs.txt") as f:
        for line in f:
            if line[0] == "#":
                continue
            if line.strip():
                (key, val) = line.split()
                if( key == 'train_vbl' ):
                    vbl_names += [val]
    
    return vbl_names

def readScalefactor() :
    scalefactor = 0.0
    with open("/public/data/hancj/ds2kkpipi0_bdt/train/dat/.scalefactor.txt") as f:
        for line in f:
            data = line.split()
            scalefactor = np.float(data[0])
    return scalefactor

def getParams(hyp) :

    hyper_params=hyp[1].split(",")

    # assign hyper-parameter values
    max_depth = int(hyper_params[0])
    min_child_weight = float(hyper_params[1])
    gamma = float(hyper_params[2])
    num_rounds = 5000
    subsample = float(hyper_params[3])
    colsample_bytree = float(hyper_params[4])
    eta = float(hyper_params[5])
    Lambda = float(hyper_params[6])
    alpha = float(hyper_params[7])
    scalefactor = readScalefactor() 

    params={
        'scale_pos_weight' : scalefactor, 
        'nthread':32,
        'max_depth':max_depth,
        'min_child_weight':min_child_weight,
        'gamma':gamma,
        'n_estimators':num_rounds,
        'eta':eta,
        'subsample':subsample,
        'colsample_bytree':colsample_bytree,
        'alpha':alpha,
        'lambda':Lambda,
    }

    # output directory
    outputdir = hyper_params[8]
    
    return params, outputdir
   

def main():

    t0 = time.time()
    
    feature_names = readFeatures()
    params, outputdir = getParams(sys.argv) 
    
    # Matrices 
    dtrain = xgb.DMatrix("/public/data/hancj/ds2kkpipi0_bdt/train/dat/train.dat",feature_names=feature_names )
    dval   = xgb.DMatrix("/public/data/hancj/ds2kkpipi0_bdt/train/dat/val.dat",feature_names=feature_names)
    dtest  = xgb.DMatrix("/public/data/hancj/ds2kkpipi0_bdt/train/dat/test.dat",feature_names=feature_names)
    
    params['eval_metric'] = 'auc'
    params['objective'] = 'binary:logistic'
    params['tree_method'] = 'hist'
    watchlist = [(dtrain,'train'),(dval,'eval')]
    evals_result = {}

    print("Runtime = " + str(time.time()-t0) + " seconds")

    # Fit the algorithm
    myboost = xgb.train(params,dtrain,params['n_estimators'],watchlist,early_stopping_rounds=10,evals_result=evals_result)

    print("Runtime = " + str(time.time()-t0) + " seconds")

    # Predict training set:
    dtrain_predictions = myboost.predict(dtrain)
    dval_predictions = myboost.predict(dval)
    dtest_predictions = myboost.predict(dtest)

    drawFeatureImportance(myboost, outputdir)
    #drawTree(myboost, outputdir)
    drawRoc(dtrain,dtrain_predictions,"training_set",
            dval,dval_predictions,"validation_set",outputdir) 
    
    drawTrainingProcess(evals_result,outputdir)
    
    tag = 0.5
    
    drawConfusionMatrix(dtest.get_label(), [(value>tag) for value in dtest_predictions], 'test_set',outputdir)
    drawConfusionMatrix(dtrain.get_label(), [(value>tag) for value in dtrain_predictions], 'training_set',outputdir)
    drawConfusionMatrix(dval.get_label(), [(value>tag) for value in dval_predictions], 'validation_set', outputdir)

    # Print model report:         
    print("\nModel Report")

    accuracy_test = metrics.accuracy_score(dtest.get_label(), [(value>tag) for value in dtest_predictions], sample_weight=dtest.get_weight())
    accuracy_train = metrics.accuracy_score(dtrain.get_label(), [(value>tag) for value in dtrain_predictions], sample_weight=dtrain.get_weight())
    accuracy_val = metrics.accuracy_score(dval.get_label(), [(value>tag) for value in dval_predictions], sample_weight=dval.get_weight())
    print("Accuracy (Test): %.2f%%" % (accuracy_test * 100.0))
    print("Accuracy (Train): %.2f%%" % (accuracy_train * 100.0))
    print("Accuracy (Validation): %.2f%%" % (accuracy_val * 100.0))

    auc_test  = metrics.roc_auc_score(dtest.get_label(), dtest_predictions, sample_weight=dtest.get_weight())
    auc_train = metrics.roc_auc_score(dtrain.get_label(), dtrain_predictions, sample_weight=dtrain.get_weight())
    auc_val   = metrics.roc_auc_score(dval.get_label(), dval_predictions, sample_weight=dval.get_weight())
    print("AUC Score (Test): %.4f" % auc_test)
    print("AUC Score (Train): %.4f" % auc_train)
    print("AUC Score (Validation): %.4f" % auc_val)

    f1 = open(outputdir+"accuracy.txt", 'w') 
    print(accuracy_test, file=f1)
    print(accuracy_train, file=f1)
    print(accuracy_val, file=f1)
    f1.close()

    f2 = open(outputdir+"auc.txt", 'w')
    print(auc_test, file=f2)
    print(auc_train, file=f2)
    print(auc_val, file=f2)
    f2.close()
 
    f4 = open("/public/data/hancj/ds2kkpipi0_bdt/train/summary.txt", 'a')
    print('{0}  {1}'.format(auc_val,outputdir), file=f4)
    f4.close()
     
   # Save model to file
    myboost.save_model(outputdir+"model_bdt.h5")
    
    print("Runtime = " + str(time.time()-t0) + " seconds")

if __name__ == "__main__":
    main()

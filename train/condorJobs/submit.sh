#########################################################################
# File Name: run.sh
# Author: Chenji Han
# mail: hanchenji16@mails.ucas.ac.cn
# Created Time: 2020年07月30日 星期四 15时25分30秒
#########################################################################
#!/bin/bash


rm  -rf ../output/*

MAX_DEPTH[1]=10
MAX_DEPTH[2]=6
MAX_DEPTH[3]=15
MAX_DEPTH[4]=20

MIN_CHILD_WEIGHT[1]=1
MIN_CHILD_WEIGHT[2]=0.3
MIN_CHILD_WEIGHT[3]=0.6
MIN_CHILD_WEIGHT[4]=1.3

GAMMA[1]=0
GAMMA[2]=0.3
GAMMA[3]=0.6
GAMMA[4]=0.9

ETA[1]=0.01
ETA[2]=0.1
ETA[3]=0.05
ETA[4]=0.01

SUBSAMPLE[1]=1
SUBSAMPLE[2]=0.7
SUBSAMPLE[3]=0.5
SUBSAMPLE[4]=0.3

COLSAMPLE_BYTREE[1]=1
COLSAMPLE_BYTREE[2]=0.7
COLSAMPLE_BYTREE[3]=0.5
COLSAMPLE_BYTREE[4]=0.3

ALPHA[1]=0
ALPHA[2]=0.3
ALPHA[3]=0.5
ALPHA[4]=0.8

LAMBDA[1]=1
LAMBDA[2]=1.3
LAMBDA[3]=1.7
LAMBDA[4]=2.0

#sh train.sh ${MAX_DEPTH[1]} ${MIN_CHILD_WEIGHT[1]} ${GAMMA[1]} ${SUBSAMPLE[1]} ${COLSAMPLE_BYTREE[1]} ${ETA[1]} ${LAMBDA[1]} ${ALPHA[1]} "../output/"


for idx1 in 1 #2 3 4
do
    for idx2 in 1 2 3  
    do
        for idx3 in 1 2 3 
        do
            for idx4 in 1 2 3 4 
            do
                for idx5 in 1 2 3 4 
                do
                    for idx6 in 1 #2 3 4
                    do
                        for idx7 in 1 2 3 4 
                        do 
                            for idx8 in 1 2 3 4 
                            do
                                hyp1=${MAX_DEPTH[${idx1}]} 
                                hyp2=${MIN_CHILD_WEIGHT[${idx2}]} 
                                hyp3=${GAMMA[${idx3}]} 
                                hyp4=${SUBSAMPLE[${idx4}]} 
                                hyp5=${COLSAMPLE_BYTREE[${idx5}]} 
                                hyp6=${ETA[${idx6}]} 
                                hyp7=${LAMBDA[${idx7}]} 
                                hyp8=${ALPHA[${idx8}]} 

                                outputdir="../output/hyp_${hyp1}_${hyp2}_${hyp3}_${hyp4}_${hyp5}_${hyp6}_${hyp7}_${hyp8}/"
                                mkdir -p $outputdir
                                jobfile="hyp_${hyp1}_${hyp2}_${hyp3}_${hyp4}_${hyp5}_${hyp6}_${hyp7}_${hyp8}.job"

cat > ${jobfile} <<EOF

Executable = train.sh
Arguments  = ${hyp1} ${hyp2} ${hyp3} ${hyp4} ${hyp5} ${hyp6} ${hyp7} ${hyp8} ${outputdir}

output     = ${outputdir}/output_${hyp1}_${hyp2}_${hyp3}_${hyp4}_${hyp5}_${hyp6}_${hyp7}_${hyp8}.out
error      = ${outputdir}/output_${hyp1}_${hyp2}_${hyp3}_${hyp4}_${hyp5}_${hyp6}_${hyp7}_${hyp8}.err
log        = ${outputdir}/output_${hyp1}_${hyp2}_${hyp3}_${hyp4}_${hyp5}_${hyp6}_${hyp7}_${hyp8}.log

Queue

EOF


                                condor_submit ${jobfile}
                                
                            done
                        done
                    done
                done
            done
        done
    done
done










/********************************************
 * From Haichen November 22, 2016
 * Modified by Jennet 
 *******************************************/

#include "analysis.hh"

int main (int argc, char **argv){
  
  if (argc < 1){
    printf("\nUsage: %s *.root\n\n",argv[0]);
    exit(0);
  }

  // Number of input arguments, excluding ./bin/cate
  int const index = argc - 1;
  
  TString path[index];
  
  for(int j = 0; j < index; j++){
    path[j] = (argv[j+1]);
  }

  // Add the input file to the chain
  TChain* ch = new TChain("output");
  for(int k = 0; k < index ; k++){
    ch->Add(path[k]);
    cout << " Adding file " << path[k] << endl;
  }
  ch->SetBranchStatus("*",1);
  
  int numev = ch->GetEntries();
  
  map<string,string> params;

  string line;
  // Read necessary input parameters from text file
  std::ifstream inputFile("inputs.txt");
  while(getline(inputFile, line)) {
    if (!line.length() || line[0] == '#')
      continue;
    std::istringstream iss(line);
    string a,b;
    iss>>a>>b;
    params[a] = b;
  }

  double lumi = stod(params["lumi"]);

  double nlep_val = stod(params["ps_nlep_val"]);
  double nbjets_val = stod(params["ps_nbjets_val"]);
  double njets_val = stod(params["ps_njets_val"]);

  int m_nlep, m_njet, m_nbjet;
  bool isPassed;
  double RNDM, weight_1ifb;
  int m_mcChannelNumber;

  double ph_pt1, ph_pt2, m_mgg;
  double BDTG;

  ch->SetBranchAddress(params["ps_nlep"].c_str(),&m_nlep);
  ch->SetBranchAddress(params["ps_njets"].c_str(),&m_njet);
  ch->SetBranchAddress(params["ps_nbjets"].c_str(),&m_nbjet);
  ch->SetBranchAddress("isPassed",&isPassed);
  ch->SetBranchAddress("RNDM",&RNDM);
  ch->SetBranchAddress(params["weight"].c_str(),&weight_1ifb);
  ch->SetBranchAddress("m_mcChannelNumber",&m_mcChannelNumber);

  ch->SetBranchAddress("BDTG",&BDTG);
  ch->SetBranchAddress("ph_pt1",&ph_pt1);
  ch->SetBranchAddress("ph_pt2",&ph_pt2);
  ch->SetBranchAddress("m_mgg",&m_mgg);

  int ncat;  
  vector<string>names;
  
  const int number = 6;
  string names0[number] = {"ttH1BDT","ttH2BDT","ttH3BDT","ttH4BDT","ttH5BDT","ttH6BDT"};
  for( int i = 0 ; i < number ; i ++) names.push_back(names0[i]);

  ncat = names.size();
  
  map<int,map<string,double> > val;

  // val is practically a counter. so initialize it by 
  // assigning a value of 0
  for( int i = 0 ; i < 10 ; i ++ ){
    for( int j = 0 ; j < ncat ; j ++ ){
      val[i][(names[j])] = 0;
    }
  }

  // Useful
  double weight = 0;

  cout << "Running cate" << endl;

  // BDT category boundaries
  vector<double> boundaries = categorization("hist_output.root");

  double B1=boundaries.at(0);
  double B2=boundaries.at(1);
  double B3=boundaries.at(2);
  double B4=boundaries.at(3);
  double B5=boundaries.at(4);
  double B6=boundaries.at(5);

  // Loop over events
  for( int i=0; i<numev; i++ ){	
    
    ch->GetEntry(i);

    if( !isPassed ) continue;

    // PRESELECTION
    if( !(m_nlep == nlep_val && 
	  m_nbjet > nbjets_val && 
	  m_njet > njets_val) )
      { continue; }

    // Relative photon pT and mass cuts
    if( !(1.0*ph_pt1/m_mgg > 0.35 && 1.0*ph_pt2/m_mgg > 0.25) )
      { continue; }
    
	
    weight = lumi*weight_1ifb;

    // ttH REGION 6
    if( BDTG > B1 && BDTG <= B2 ){
      yield(val,"ttH6BDT",weight,m_mcChannelNumber);
      continue;
    }
    // ttH REGION 5                                                                                                 
    if( BDTG > B2 && BDTG <= B3){
      yield(val,"ttH5BDT",weight,m_mcChannelNumber);
      continue;
    }
    // ttH REGION 4                                                                                                 
    if( BDTG > B3 && BDTG <= B4 ){
      yield(val,"ttH4BDT",weight,m_mcChannelNumber);
      continue;
    }
    // ttH REGION 3  
    if( BDTG > B4 && BDTG <= B5 ){
      yield(val,"ttH3BDT",weight,m_mcChannelNumber);
      continue;
    }
    // ttH REGION 2
    if( BDTG > B5 && BDTG <= B6 ){
      yield(val,"ttH2BDT",weight,m_mcChannelNumber);
      continue;
    }
    // ttH REGION 1
    if( BDTG > B6 ){
      yield(val,"ttH1BDT",weight,m_mcChannelNumber);
      continue;
    }
    
  } // End loop over events
  
  write_output(val,names,"fullMyy/");

  return 0 ;
}




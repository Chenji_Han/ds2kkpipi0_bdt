/********************************************
 * From Haichen November 22, 2016
 * Modified by Jennet 
 *******************************************/

#include "analysis.hh"

int main (int argc, char **argv){
  
  if (argc < 1){
    printf("\nUsage: %s *.root\n\n",argv[0]);
    exit(0);
  }

  // Number of input arguments, excluding ./bin/cate
  int const index = argc - 1;
  
  TString path[index];
  
  for(int j = 0; j < index; j++){
    path[j] = (argv[j+1]);
  }

  // Add the input file to the chain
  TChain* ch = new TChain("output");
  for(int k = 0; k < index ; k++){
    ch->Add(path[k]);
    cout << " Adding file " << path[k] << endl;
  }
  ch->SetBranchStatus("*",1);
  
  int numev = ch->GetEntries();
  
  map<string,string> params;

  string line;
  // Read necessary input parameters from text file
  std::ifstream inputFile("inputs.txt");
  while(getline(inputFile, line)) {
    if (!line.length() || line[0] == '#')
      continue;
    std::istringstream iss(line);
    string a,b;
    iss>>a>>b;
    params[a] = b;
  }

  double lumi = stod(params["lumi"]);

  // Scale factor calculation done with
  // macros/scalefactor.C
  double scalefactor_not_TITI = stod(params["scalefactor_ana_nti"]);
  double scalefactor_TITI = stod(params["scalefactor_ana_ti"]);
  double masswin = stod(params["masswin_ana"]);

  double nlep_val = stod(params["ps_nlep_val"]);
  double nbjets_val = stod(params["ps_nbjets_val"]);
  double njets_val = stod(params["ps_njets_val"]);

  int m_nlep, m_njet, m_nbjet;
  bool flag_passedPID, flag_passedIso;
  double weight_1ifb;
  int m_mcChannelNumber;
  int fold;

  double ph_pt1, ph_pt2, m_mgg;
  double BDTG;

  ch->SetBranchAddress(params["ps_nlep"].c_str(),&m_nlep);
  ch->SetBranchAddress(params["ps_njets"].c_str(),&m_njet);
  ch->SetBranchAddress(params["ps_nbjets"].c_str(),&m_nbjet);
  ch->SetBranchAddress("flag_passedPID",&flag_passedPID);
  ch->SetBranchAddress("flag_passedIso",&flag_passedIso);
  ch->SetBranchAddress("fold",&fold);
  ch->SetBranchAddress(params["weight"].c_str(),&weight_1ifb);
  ch->SetBranchAddress("m_mcChannelNumber",&m_mcChannelNumber);

  string name = params["BDTG"]+"0";
  ch->SetBranchAddress(name.c_str(),&BDTG);
  ch->SetBranchAddress("ph_pt1",&ph_pt1);
  ch->SetBranchAddress("ph_pt2",&ph_pt2);
  ch->SetBranchAddress("m_mgg",&m_mgg);

  int ncat;  
  vector<string>names;
  
  const int number = 6;
  string names0[number] = {"ttH1BDT","ttH2BDT","ttH3BDT","ttH4BDT","ttH5BDT","ttH6BDT"};
  for( int i = 0 ; i < number ; i ++) names.push_back(names0[i]);

  ncat = names.size();
  
  map<int,map<string,double> > val;
  
  // val is practically a counter. so initialize it by 
  // assigning a value of 0
  for( int i = 0 ; i < 8 ; i ++ ){
    for( int j = 0 ; j < ncat ; j ++ ){
      val[i][(names[j])] = 0 ;
    }
  }

  // Useful
  double weight = 0;

  // Histograms of BDT score by sample
  // Training sample
  TH1F* h_tr_BDT_ttH  = new TH1F("h_tr_BDT_ttH", "h_tr_BDT_ttH", 1000,0,1);
  TH1F* h_tr_BDT_ggH  = new TH1F("h_tr_BDT_ggH", "h_tr_BDT_ggH", 1000,0,1);
  TH1F* h_tr_BDT_bkg  = new TH1F("h_tr_BDT_bkg", "h_tr_BDT_bkg", 1000,0,1);

  // Testing sample
  TH1F* h_te_BDT_ttH  = new TH1F("h_te_BDT_ttH", "h_te_BDT_ttH", 1000,0,1);
  TH1F* h_te_BDT_ggH  = new TH1F("h_te_BDT_ggH", "h_te_BDT_ggH", 1000,0,1);
  TH1F* h_te_BDT_bkg  = new TH1F("h_te_BDT_bkg", "h_te_BDT_bkg", 1000,0,1);

  // Validation sample
  TH1F* h_va_BDT_bkg  = new TH1F("h_va_BDT_bkg", "h_va_BDT_bkg", 1000,0,1);

  // TI sample
  TH1F* h_ti_BDT_bkg  = new TH1F("h_ti_BDT_bkg", "h_ti_BDT_bkg", 1000,0,1);

  // All others
  TH1F* h_te_BDT_tHjb = new TH1F("h_te_BDT_tHjb","h_te_BDT_tHjb", 1000,0,1); 
  TH1F* h_te_BDT_tWH  = new TH1F("h_te_BDT_tWH", "h_te_BDT_tWH",  1000,0,1);
  TH1F* h_te_BDT_VBF  = new TH1F("h_te_BDT_VBF", "h_te_BDT_VBF",  1000,0,1);
  TH1F* h_te_BDT_ZH   = new TH1F("h_te_BDT_ZH",  "h_te_BDT_ZH",   1000,0,1);
  TH1F* h_te_BDT_WH   = new TH1F("h_te_BDT_WH",  "h_te_BDT_WH",   1000,0,1);
  TH1F* h_te_BDT_ggZH = new TH1F("h_te_BDT_ggZH", "h_te_BDT_ggZH", 1000,0,1);
  TH1F* h_te_BDT_bbH = new TH1F("h_te_BDT_bbH",  "h_te_BDT_bbH",  1000,0,1);

  cout << "Running cate_hists" << endl;

  // MC channel numbers
  int i_ttH = stod(params["i_ttH"]);
  int i_ggH = stod(params["i_ggH"]);
  int i_tWH = stod(params["i_tWH"]);
  int i_tHjb = stod(params["i_tHjb"]);
  int i_VBF = stod(params["i_VBF"]);
  int i_ZH = stod(params["i_ZH"]);
  int i_WH = stod(params["i_WH"]);
  int i_WH2 = i_WH+1;
  int i_ggZH = stod(params["i_ggZH"]);
  int i_bbH = stod(params["i_bbH"]);

  // Loop over events
  for( int i=0; i<numev; i++ ){	
    
    ch->GetEntry(i);

    // PRESELECTION
    if( !(m_nlep == nlep_val && 
	  m_nbjet > nbjets_val && 
	  m_njet > njets_val) )
      { continue; }

    // Relative photon pT and mass cuts
    if( !(1.0*ph_pt1/m_mgg > 0.35 && 1.0*ph_pt2/m_mgg > 0.25) )
      { continue; }
    
    // Mass cuts
    if( m_mgg < 105 || m_mgg > 160 )
      { continue; }
    
    // If is MC sample
    if( m_mcChannelNumber > 0 &&
	fabs(m_mgg - 125) < masswin && 
	flag_passedIso &&
	flag_passedPID ){
      
      // First deal with samples not involved in training
      if( m_mcChannelNumber == i_tHjb ||
	  m_mcChannelNumber == i_tWH ||
	  m_mcChannelNumber == i_VBF ||
	  m_mcChannelNumber == i_WH ||
	  m_mcChannelNumber == i_WH2 ||
	  m_mcChannelNumber == i_ZH ){
	
	weight = lumi*weight_1ifb;

	// tHjb                                                                                                       
	if( m_mcChannelNumber == i_tHjb ){ 
	  h_te_BDT_tHjb-> Fill(BDTG, weight); 
	}
	// tWH
	if( m_mcChannelNumber == i_tWH ){
	  h_te_BDT_tWH-> Fill(BDTG, weight);
	}
	// VBF                                                                                               
	if( m_mcChannelNumber == i_VBF ){
	  h_te_BDT_VBF-> Fill(BDTG, weight);
	}
	// WH                                                                                                    
	if( m_mcChannelNumber == i_WH || m_mcChannelNumber == i_WH2 ){
	  h_te_BDT_WH-> Fill(BDTG, weight);
	}
	// ZH                                                                                                  
	if( m_mcChannelNumber == i_ZH ){
	  h_te_BDT_ZH-> Fill(BDTG, weight);
	}
        // ggZH
        if( m_mcChannelNumber == i_ggZH ){
          h_te_BDT_ggZH-> Fill(BDTG, weight);
        }
        // bbH
        if( m_mcChannelNumber == i_bbH ){
          h_te_BDT_bbH-> Fill(BDTG, weight);
        }
      } // End MC not used for training
      
      // Now MC used for training
      if( m_mcChannelNumber == i_ttH ||
	  m_mcChannelNumber == i_ggH ){
	weight = lumi*weight_1ifb;
	
	if( fold==1 && m_mcChannelNumber == i_ttH ) h_te_BDT_ttH->Fill(BDTG, 4*weight);
	if( (fold==2 || fold==3) && m_mcChannelNumber == i_ttH ) h_tr_BDT_ttH->Fill(BDTG, 2*weight);
        if( fold==1 && m_mcChannelNumber == i_ggH ) h_te_BDT_ggH->Fill(BDTG, 4*weight);
        if( (fold==2 || fold==3)  && m_mcChannelNumber == i_ggH ) h_tr_BDT_ggH->Fill(BDTG, 2*weight);

      } // End if MC used for training
    } // End if MC 
    
    // If data 
    if( m_mcChannelNumber <= 0 &&
	fabs(m_mgg - 125) > 5 ){
      
      if( !(flag_passedIso && flag_passedPID) ){

	double weight0 = scalefactor_not_TITI*lumi*weight_1ifb;

	// If testing sample
	if( fold==2 || fold==3 ){	  
	  weight = 4*weight0;
	  h_te_BDT_bkg->Fill(BDTG, weight);
	}

	// If validation sample
	if( fold==0 ){
	  weight = 4*weight0;
	  h_va_BDT_bkg->Fill(BDTG, weight);
	}
	
	// If test sample
	if( fold==1 ){
	  weight = 2*weight0;
	  h_tr_BDT_bkg->Fill(BDTG, weight);	  
	} 
      } // End if NTNI
      
      // If TI
      if( flag_passedIso && flag_passedPID ){
	weight = scalefactor_TITI*lumi*weight_1ifb;
	h_ti_BDT_bkg->Fill(BDTG, weight);
      } // End if TI
    } // End if data
    
  } // End loop over events
    
  TFile f("hist_output.root","recreate");
  h_tr_BDT_ttH->Write();
  h_tr_BDT_ggH->Write();
  h_tr_BDT_bkg->Write();

  h_te_BDT_ttH->Write();
  h_te_BDT_tWH->Write();
  h_te_BDT_tHjb->Write();
  h_te_BDT_ggH->Write();
  h_te_BDT_bkg->Write();
  h_te_BDT_VBF->Write();
  h_te_BDT_WH->Write();
  h_te_BDT_ZH->Write();
  h_te_BDT_ggZH->Write();
  h_te_BDT_bbH->Write();

  h_va_BDT_bkg->Write();
  h_ti_BDT_bkg->Write();

  f.Close();
  
  return 0 ;
}




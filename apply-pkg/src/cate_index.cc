/********************************************
 * From Haichen November 22, 2016
 * Modified by Jennet 
 *******************************************/

#include "analysis.hh"

int main (int argc, char **argv){

  cout << "Creating tree" << endl;

  if( argc < 2 || argc > 2 ){
    cout << "Please specify one input file" << endl;
    return 0;
  }

  // Create output file
  string gen = string(argv[1]);
  string hist_filename = gen + "rwi.root";
  TFile outfile(hist_filename.c_str(),"recreate");

  // Add the input file to the chain
  TChain* ch = new TChain("output");
  ch->AddFile((gen+"rw.root").c_str());
  cout << "Input file " << gen+"rw.root" << endl;

  ch->SetBranchStatus("*",1);
  
  int numev = ch->GetEntries();
  
  bool flag_passedPID, flag_passedIso;
  double BDTG;

  ch->SetBranchAddress("flag_passedPID",&flag_passedPID);
  ch->SetBranchAddress("flag_passedIso",&flag_passedIso);
  ch->SetBranchAddress("BDTG",&BDTG);

  TTree* ch_new = (TTree*)ch->CloneTree(0);
  int BDTG_index;
  double dummy = 1, dummy2 = 1;
  ch_new->Branch("BDTG_index",&BDTG_index);
  ch_new->Branch("dummy",&dummy);
  ch_new->Branch("dummy2",&dummy2);

  cout << "Running cate_index" << endl;

  // BDT category boundaries
  double B1, B2, B3, B4, B5, B6;

  string line;
  std::ifstream inputFile2("boundaries.txt");
  while(getline(inputFile2, line)) {
    if (!line.length() || line[0] == '#')
      continue;
    std::istringstream iss(line);
    string a,b,c,d,e,f;
    iss>>a>>b>>c>>d>>e>>f;
    B1 = stod(a);
    B2 = stod(b);
    B3 = stod(c);
    B4 = stod(d);
    B5 = stod(e);
    B6 = stod(f);
  }

  TH1D* h = new TH1D("CutFlow_weighted_noDalitz","CutFlow_weighted_noDalitz",1,0,1);
  h->Fill(.5);

  // Loop over events
  for( int i=0; i<numev; i++ ){	
    
    ch->GetEntry(i);

    // PRESELECTION
    if( !(flag_passedPID && flag_passedIso) ) continue;

    BDTG_index = 0;
    
    // ttH REGION 1
    if( BDTG > B1 && BDTG <= B2 ){
      BDTG_index = 6;
    }
    // ttH REGION 2
    else if( BDTG > B2 && BDTG <= B3){
      BDTG_index = 5;
    }
    // ttH REGION 3
    else if( BDTG > B3 && BDTG <= B4 ){
      BDTG_index = 4;
    }
    // ttH REGION 4
    else if( BDTG > B4 && BDTG <= B5 ){
      BDTG_index = 3;
    }
    // ttH REGION 5
    else if( BDTG > B5 && BDTG <= B6 ){
      BDTG_index = 2;
    }
    // ttH REGION 6
    else if( BDTG > B6 ){
      BDTG_index = 1;
    }
    
    ch_new->Fill();
  } // End loop over events

  cout << "Writing output" << endl;
  ch_new->Write();
  h->Write();
  outfile.Close();
  
  return 0 ;
}




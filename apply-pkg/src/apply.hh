#include "TGraphAsymmErrors.h"
#include "TEfficiency.h"
#include "TLatex.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "TROOT.h"
#include "THStack.h"
#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "RooRealVar.h"
#include "RooExponential.h"
#include "RooPlot.h"
#include "RooGaussian.h"
#include "RooPolynomial.h"
#include "RooAddPdf.h"
#include "RooAbsData.h"
#include "RooAbsRealLValue.h"
#include "RooAbsPdf.h"
#include "RooMinuit.h"
#include "RooCategory.h"
#include "RooDataSet.h"
#include "RooArgSet.h"
#include "RooDataHist.h"
#include "RooNLLVar.h"
#include "RooSimultaneous.h"
#include "RooExponential.h"
#include "RooGlobalFunc.h"
#include "RooCBShape.h"
#include "RooFormula.h"
#include "RooRandom.h"
#include "RooFitResult.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>

#include "header.hh"
using namespace std;
using namespace RooFit;
#include <sys/stat.h>

double significance( double s , double b)
{
  if( s > 0 && b > 0 )
    return sqrt(2*((s+b)*log(1+s/b)-s));
  // see eq 97 of  https://arxiv.org/pdf/1007.1727v3.pdf
  else
    return 0;
}

/***************************************************
 * Tests whether a file exists.
 ***************************************************/
bool file_exists(string name) {
  ifstream f(name.c_str());
  if (f.good()) {
    f.close();
    return true;
  } else {
    f.close();
    return false;
  }
}

/***************************************************
 * Returns indices of vector in sorted order
 ***************************************************/
vector<int> sort_index(const vector<double> &v) {

  // initialize original index locations
  vector<int> idx(v.size());
  for (size_t i = 0; i != idx.size(); ++i) idx.at(i) = i;

  // sort indexes based on comBparing values in v
  sort(idx.begin(), idx.end(),
       [&v](size_t i1, size_t i2) {return v.at(i1) > v.at(i2);});

  return idx;
}

map<string,string> get_params(){

  map<string,string> params;
  string line;

  // Read necessary input parameters from text file
  std::ifstream inputFile("inputs.txt");
  while(getline(inputFile, line)) {
    if (!line.length() || line[0] == '#')
      continue;
    std::istringstream iss(line);
    string a,b;
    iss>>a>>b;

    if( a != "train_vbl" ){
      params[a] = b;
    }
  }
  inputFile.close();

  return params;
}

vector<string> get_vbls(){

  vector<string> vbls;

  string line;
  // Read necessary input parameters from text file
  std::ifstream inputFile("inputs.txt");
  while(getline(inputFile, line)) {
    if (!line.length() || line[0] == '#')
      continue;
    std::istringstream iss(line);
    string a,b;
    iss>>a>>b;

    if( a == "train_vbl" ){
      vbls.push_back(b);
    }
  }
  inputFile.close();

  return vbls;
}

double get_prob(double score, TH1D* h){

  int bin = h->GetXaxis()->FindBin(score);
  
  double numerator = h->Integral(1,bin);
  double denominator = h->Integral();

  return 1.0*numerator/denominator;
}

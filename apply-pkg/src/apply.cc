/********************************************                    
 * Jennet Dickinson
 * August 14, 2018
 ********************************************/

#include "apply.hh"
#include "xgboost/c_api.h"

int main (int argc, char **argv){

  cout << "Creating tree" << endl;

  if( argc < 2 || argc > 2 ){
    cout << "Please specify one input file" << endl;
    return 0;
  }
  
  string model = "model_bdt.h5";
  string gen = string(argv[1]);

  map<string,string> params = get_params();
  vector<string> vbls = get_vbls();
  vector<double> vbl_vals(vbls.size(),0);
  vector<int> vbl_vals_int(vbls.size(),0);

  // Create output file
  string filename = gen + "w.root";
  TFile outfile(filename.c_str(),"recreate");

  BoosterHandle* myboost = new BoosterHandle();
  XGBoosterCreate(0, 0, * &myboost);
  XGBoosterLoadModel(*myboost, model.c_str());
  DMatrixHandle dmat;

  // Add the input file to the chain
  TChain* ch = new TChain("output");
  ch->AddFile(("../sample/rootSample/"+gen+".root").c_str());
  cout << "Input file " << "../sample/"+gen+".root" << endl;

  ch->SetBranchStatus("*",1);
  for(int i=0; i<int(vbls.size()); i++){
    ch->SetBranchAddress(vbls.at(i).c_str(),&vbl_vals.at(i));
  }

  TTree* ch_new = (TTree*)ch->CloneTree(0);
  double BDTG;
  ch_new->Branch("BDTG",&BDTG);

  const int len = vbls.size();
  float vars[len];

  // Begin loop over events
  for(int i=0; i<ch->GetEntries(); i++){
    
    ch->GetEntry(i);
    if( i%10000 == 0 ){
      cout << "Processed " << 100.0*i/ch->GetEntries() << "% of events" << endl;
    }

    for(int j=0; j<len; j++){
      vars[j] = vbl_vals.at(j);
    } // End loop over variables

    XGDMatrixCreateFromMat(vars, 1, len, -1, &dmat);
    bst_ulong out_len;
    const float *f;
    XGBoosterPredict(*myboost, dmat, 0, 0, &out_len, &f);
    XGDMatrixFree(dmat);
    
    BDTG = *f;
    ch_new->Fill();
  } // End loop over events

  cout << "Writing output" << endl;
  ch_new->Write();
  outfile.Close();
  
  return 0;
}

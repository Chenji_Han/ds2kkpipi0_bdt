/********************************************
 * From Haichen November 22, 2016
 * Modified by Jennet 
 *******************************************/

#include "analysis.hh"

int main (int argc, char **argv){
  
  if (argc < 1){
    printf("\nUsage: %s *.root\n\n",argv[0]);
    exit(0);
  }

  // Number of input arguments, excluding ./bin/cate
  int const index = argc - 1;
  
  TString path[index];
  
  for(int j = 0; j < index; j++){
    path[j] = (argv[j+1]);
  }

  // Add the input file to the chain
  TChain* ch = new TChain("output");
  for(int k = 0; k < index ; k++){
    ch->Add(path[k]);
    cout << " Adding file " << path[k] << endl;
  }
  ch->SetBranchStatus("*",1);
  
  int numev = ch->GetEntries();
  
  map<string,string> params;

  string line;
  // Read necessary input parameters from text file
  std::ifstream inputFile("inputs.txt");
  while(getline(inputFile, line)) {
    if (!line.length() || line[0] == '#')
      continue;
    std::istringstream iss(line);
    string a,b;
    iss>>a>>b;
    params[a] = b;
  }

  double lumi = stod(params["lumi"]);

  // Scale factor calculation done with
  // macros/scalefactor.C
  double scalefactor_not_TITI = stod(params["scalefactor_ana_nti"]);
  double scalefactor_TITI = stod(params["scalefactor_ana_ti"]);
  double masswin = stod(params["masswin_ana"]);

  double nlep_val = stod(params["ps_nlep_val"]);
  double nbjets_val = stod(params["ps_nbjets_val"]);
  double njets_val = stod(params["ps_njets_val"]);

  int m_nlep, m_njet, m_nbjet;
  bool flag_passedPID, flag_passedIso;
  double weight_1ifb;
  int m_mcChannelNumber;
  int fold;

  double ph_pt1, ph_pt2, m_mgg;
  double BDTG;

  ch->SetBranchAddress(params["ps_nlep"].c_str(),&m_nlep);
  ch->SetBranchAddress(params["ps_njets"].c_str(),&m_njet);
  ch->SetBranchAddress(params["ps_nbjets"].c_str(),&m_nbjet);
  ch->SetBranchAddress("flag_passedPID",&flag_passedPID);
  ch->SetBranchAddress("flag_passedIso",&flag_passedIso);
  ch->SetBranchAddress("fold",&fold);
  ch->SetBranchAddress(params["weight"].c_str(),&weight_1ifb);
  ch->SetBranchAddress("m_mcChannelNumber",&m_mcChannelNumber);

  string name = params["BDTG"]+"0";
  ch->SetBranchAddress(name.c_str(),&BDTG);
  ch->SetBranchAddress("ph_pt1",&ph_pt1);
  ch->SetBranchAddress("ph_pt2",&ph_pt2);
  ch->SetBranchAddress("m_mgg",&m_mgg);

  int ncat;  
  vector<string>names;
  
  const int number = 6;
  string names0[number] = {"ttH1BDT","ttH2BDT","ttH3BDT","ttH4BDT","ttH5BDT","ttH6BDT"};
  for( int i = 0 ; i < number ; i ++) names.push_back(names0[i]);

  ncat = names.size();
  
  map<int,map<string,double> > val;
  map<int,map<string,double> > val_tr;
  map<int,map<string,double> > val_va;
  map<int,map<string,double> > val_ti;  

  // val is practically a counter. so initialize it by 
  // assigning a value of 0
  for( int i = 0 ; i < 10 ; i ++ ){
    for( int j = 0 ; j < ncat ; j ++ ){
      val[i][(names[j])] = 0;
      val_tr[i][(names[j])] = 0;
      val_va[i][(names[j])] = 0;
      val_ti[i][(names[j])] = 0;
    }
  }

  // Useful
  double weight = 0;

  // Histograms of BDT score by sample
  TH1F* h_tr_ttHBDT1 = new TH1F("h_tr_ttHBDT1", "h_tr_ttHBDT1", 8,0,8);
  TH1F* h_tr_ttHBDT2 = new TH1F("h_tr_ttHBDT2", "h_tr_ttHBDT2", 8,0,8);
  TH1F* h_tr_ttHBDT3 = new TH1F("h_tr_ttHBDT3", "h_tr_ttHBDT3", 8,0,8);
  TH1F* h_tr_ttHBDT4 = new TH1F("h_tr_ttHBDT4", "h_tr_ttHBDT4", 8,0,8);
  TH1F* h_tr_ttHBDT5 = new TH1F("h_tr_ttHBDT5", "h_tr_ttHBDT5", 8,0,8);
  TH1F* h_tr_ttHBDT6 = new TH1F("h_tr_ttHBDT6", "h_tr_ttHBDT6", 8,0,8);

  TH1F* h_te_ttHBDT1 = new TH1F("h_te_ttHBDT1", "h_te_ttHBDT1", 8,0,8);
  TH1F* h_te_ttHBDT2 = new TH1F("h_te_ttHBDT2", "h_te_ttHBDT2", 8,0,8);
  TH1F* h_te_ttHBDT3 = new TH1F("h_te_ttHBDT3", "h_te_ttHBDT3", 8,0,8);
  TH1F* h_te_ttHBDT4 = new TH1F("h_te_ttHBDT4", "h_te_ttHBDT4", 8,0,8);
  TH1F* h_te_ttHBDT5 = new TH1F("h_te_ttHBDT5", "h_te_ttHBDT5", 8,0,8);
  TH1F* h_te_ttHBDT6 = new TH1F("h_te_ttHBDT6", "h_te_ttHBDT6", 8,0,8);

  TH1F* h_ti_ttHBDT1 = new TH1F("h_ti_ttHBDT1", "h_ti_ttHBDT1", 8,0,8);
  TH1F* h_ti_ttHBDT2 = new TH1F("h_ti_ttHBDT2", "h_ti_ttHBDT2", 8,0,8);
  TH1F* h_ti_ttHBDT3 = new TH1F("h_ti_ttHBDT3", "h_ti_ttHBDT3", 8,0,8);
  TH1F* h_ti_ttHBDT4 = new TH1F("h_ti_ttHBDT4", "h_ti_ttHBDT4", 8,0,8);
  TH1F* h_ti_ttHBDT5 = new TH1F("h_ti_ttHBDT5", "h_ti_ttHBDT5", 8,0,8);
  TH1F* h_ti_ttHBDT6 = new TH1F("h_ti_ttHBDT6", "h_ti_ttHBDT6", 8,0,8);

  TH1F* h_va_ttHBDT1 = new TH1F("h_va_ttHBDT1", "h_va_ttHBDT1", 8,0,8);
  TH1F* h_va_ttHBDT2 = new TH1F("h_va_ttHBDT2", "h_va_ttHBDT2", 8,0,8);
  TH1F* h_va_ttHBDT3 = new TH1F("h_va_ttHBDT3", "h_va_ttHBDT3", 8,0,8);
  TH1F* h_va_ttHBDT4 = new TH1F("h_va_ttHBDT4", "h_va_ttHBDT4", 8,0,8);
  TH1F* h_va_ttHBDT5 = new TH1F("h_va_ttHBDT5", "h_va_ttHBDT5", 8,0,8);
  TH1F* h_va_ttHBDT6 = new TH1F("h_va_ttHBDT6", "h_va_ttHBDT6", 8,0,8);

  setup_hist(h_tr_ttHBDT1);
  setup_hist(h_tr_ttHBDT2);
  setup_hist(h_tr_ttHBDT3);
  setup_hist(h_tr_ttHBDT4);
  setup_hist(h_tr_ttHBDT5);
  setup_hist(h_tr_ttHBDT6);

  setup_hist(h_te_ttHBDT1);
  setup_hist(h_te_ttHBDT2);
  setup_hist(h_te_ttHBDT3);
  setup_hist(h_te_ttHBDT4);
  setup_hist(h_te_ttHBDT5);
  setup_hist(h_te_ttHBDT6);

  setup_hist(h_va_ttHBDT1);
  setup_hist(h_va_ttHBDT2);
  setup_hist(h_va_ttHBDT3);
  setup_hist(h_va_ttHBDT4);
  setup_hist(h_va_ttHBDT5);
  setup_hist(h_va_ttHBDT6);

  setup_hist(h_ti_ttHBDT1);
  setup_hist(h_ti_ttHBDT2);
  setup_hist(h_ti_ttHBDT3);
  setup_hist(h_ti_ttHBDT4);
  setup_hist(h_ti_ttHBDT5);
  setup_hist(h_ti_ttHBDT6);

  cout << "Running cate" << endl;

  // MC channel numbers
  int i_ttH = stod(params["i_ttH"]);
  int i_ggH = stod(params["i_ggH"]);
  int i_tWH = stod(params["i_tWH"]);
  int i_tHjb = stod(params["i_tHjb"]);
  int i_VBF = stod(params["i_VBF"]);
  int i_ZH = stod(params["i_ZH"]);
  int i_WH = stod(params["i_WH"]);
  int i_WH2 = i_WH+1;
  int i_ggZH = stod(params["i_ggZH"]);
  int i_bbH = stod(params["i_bbH"]);

  // BDT category boundaries
  vector<double> boundaries = categorization("hist_output.root");

  double B1=boundaries.at(0);
  double B2=boundaries.at(1);
  double B3=boundaries.at(2);
  double B4=boundaries.at(3);
  double B5=boundaries.at(4);
  double B6=boundaries.at(5);

  // Loop over events
  for( int i=0; i<numev; i++ ){	
    
    ch->GetEntry(i);

    // PRESELECTION
    if( !(m_nlep == nlep_val && 
	  m_nbjet > nbjets_val && 
	  m_njet > njets_val) )
      { continue; }

    // Relative photon pT and mass cuts
    if( !(1.0*ph_pt1/m_mgg > 0.35 && 1.0*ph_pt2/m_mgg > 0.25) )
      { continue; }
    
    // Mass cuts
    if( m_mgg < 105 || m_mgg > 160 )
      { continue; }
    
    // If is MC sample
    if( m_mcChannelNumber > 0 &&
	fabs(m_mgg - 125) < masswin && 
	flag_passedIso &&
	flag_passedPID ){
      
      // First deal with samples not involved in training
      if( m_mcChannelNumber == i_tHjb ||
	  m_mcChannelNumber == i_tWH ||
	  m_mcChannelNumber == i_VBF ||
	  m_mcChannelNumber == i_WH ||
	  m_mcChannelNumber == i_WH2 ||
	  m_mcChannelNumber == i_ZH || 
	  m_mcChannelNumber == i_ggZH ||
	  m_mcChannelNumber == i_bbH ){
	
	weight = lumi*weight_1ifb;

	// ttH REGION 6
	if( BDTG > B1 && BDTG <= B2 ){
	  yield(val,"ttH6BDT",weight,m_mcChannelNumber);
	  yield(val_tr,"ttH6BDT",weight,m_mcChannelNumber);
	  yield(val_va,"ttH6BDT",weight,m_mcChannelNumber);
	  yield(val_ti,"ttH6BDT",weight,m_mcChannelNumber);

	  if( m_mcChannelNumber == i_tHjb ) h_te_ttHBDT6->Fill("tHjb",weight);
	  if( m_mcChannelNumber == i_tWH )  h_te_ttHBDT6->Fill("tWH", weight);
	  if( m_mcChannelNumber == i_VBF )  h_te_ttHBDT6->Fill("VBF", weight);
	  if( m_mcChannelNumber == i_WH || m_mcChannelNumber == i_WH2 )   h_te_ttHBDT6->Fill("WH",  weight);
	  if( m_mcChannelNumber == i_ZH )   h_te_ttHBDT6->Fill("ZH",  weight);
	  if( m_mcChannelNumber == i_ggZH ) h_te_ttHBDT6->Fill("ggZH",weight);
	  if( m_mcChannelNumber == i_bbH )  h_te_ttHBDT6->Fill("bbH", weight);

	  continue;
	}
	// ttH REGION 5                                                                                                 
	if( BDTG > B2 && BDTG <= B3){
	  yield(val,"ttH5BDT",weight,m_mcChannelNumber);
	  yield(val_tr,"ttH5BDT",weight,m_mcChannelNumber);
	  yield(val_va,"ttH5BDT",weight,m_mcChannelNumber);
	  yield(val_ti,"ttH5BDT",weight,m_mcChannelNumber);

	  if( m_mcChannelNumber == i_tHjb ) h_te_ttHBDT5->Fill("tHjb",weight);	    
	  if( m_mcChannelNumber == i_tWH )  h_te_ttHBDT5->Fill("tWH", weight);
	  if( m_mcChannelNumber == i_VBF )  h_te_ttHBDT5->Fill("VBF", weight);
	  if( m_mcChannelNumber == i_WH || m_mcChannelNumber == i_WH2 )   h_te_ttHBDT5->Fill("WH",  weight);
	  if( m_mcChannelNumber == i_ZH )   h_te_ttHBDT5->Fill("ZH",  weight);
	  if( m_mcChannelNumber == i_ggZH ) h_te_ttHBDT5->Fill("ggZH",weight);
          if( m_mcChannelNumber == i_bbH )  h_te_ttHBDT5->Fill("bbH", weight);

	  continue;
	}
	// ttH REGION 4                                                                                                 
	if( BDTG > B3 && BDTG <= B4 ){
	  yield(val,"ttH4BDT",weight,m_mcChannelNumber);
	  yield(val_tr,"ttH4BDT",weight,m_mcChannelNumber);
	  yield(val_va,"ttH4BDT",weight,m_mcChannelNumber);
	  yield(val_ti,"ttH4BDT",weight,m_mcChannelNumber);

	  if( m_mcChannelNumber == i_tHjb ) h_te_ttHBDT4->Fill("tHjb",weight);
	  if( m_mcChannelNumber == i_tWH )  h_te_ttHBDT4->Fill("tWH", weight);
	  if( m_mcChannelNumber == i_VBF )  h_te_ttHBDT4->Fill("VBF", weight);
	  if( m_mcChannelNumber == i_WH || m_mcChannelNumber == i_WH2 )   h_te_ttHBDT4->Fill("WH",  weight);
	  if( m_mcChannelNumber == i_ZH )   h_te_ttHBDT4->Fill("ZH",  weight);
          if( m_mcChannelNumber == i_ggZH ) h_te_ttHBDT4->Fill("ggZH",weight);
          if( m_mcChannelNumber == i_bbH )  h_te_ttHBDT4->Fill("bbH", weight);
	  
	  continue;
	}
	// ttH REGION 3  
	if( BDTG > B4 && BDTG <= B5 ){
	  yield(val,"ttH3BDT",weight,m_mcChannelNumber);
	  yield(val_tr,"ttH3BDT",weight,m_mcChannelNumber);
	  yield(val_va,"ttH3BDT",weight,m_mcChannelNumber);
	  yield(val_ti,"ttH3BDT",weight,m_mcChannelNumber);

	  if( m_mcChannelNumber == i_tHjb ) h_te_ttHBDT3->Fill("tHjb",weight);
	  if( m_mcChannelNumber == i_tWH )  h_te_ttHBDT3->Fill("tWH", weight);
	  if( m_mcChannelNumber == i_VBF )  h_te_ttHBDT3->Fill("VBF", weight);
	  if( m_mcChannelNumber == i_WH || m_mcChannelNumber == i_WH2 )   h_te_ttHBDT3->Fill("WH",  weight);
	  if( m_mcChannelNumber == i_ZH )   h_te_ttHBDT3->Fill("ZH",  weight);
	  if( m_mcChannelNumber == i_ggZH ) h_te_ttHBDT3->Fill("ggZH",weight);
          if( m_mcChannelNumber == i_bbH )  h_te_ttHBDT3->Fill("bbH", weight);

	  continue;
	}
	// ttH REGION 2
        if( BDTG > B5 && BDTG <= B6 ){
          yield(val,"ttH2BDT",weight,m_mcChannelNumber);
	  yield(val_tr,"ttH2BDT",weight,m_mcChannelNumber);
	  yield(val_va,"ttH2BDT",weight,m_mcChannelNumber);
	  yield(val_ti,"ttH2BDT",weight,m_mcChannelNumber);

          if( m_mcChannelNumber == i_tHjb ) h_te_ttHBDT2->Fill("tHjb",weight);
          if( m_mcChannelNumber == i_tWH )  h_te_ttHBDT2->Fill("tWH", weight);
          if( m_mcChannelNumber == i_VBF )  h_te_ttHBDT2->Fill("VBF", weight);
          if( m_mcChannelNumber == i_WH || m_mcChannelNumber == i_WH2 )   h_te_ttHBDT2->Fill("WH",  weight);
          if( m_mcChannelNumber == i_ZH )   h_te_ttHBDT2->Fill("ZH",  weight);
          if( m_mcChannelNumber == i_ggZH ) h_te_ttHBDT2->Fill("ggZH",weight);
          if( m_mcChannelNumber == i_bbH )  h_te_ttHBDT2->Fill("bbH", weight);

          continue;
        }
	// ttH REGION 1
        if( BDTG > B6 ){
          yield(val,"ttH1BDT",weight,m_mcChannelNumber);
	  yield(val_tr,"ttH1BDT",weight,m_mcChannelNumber);
	  yield(val_va,"ttH1BDT",weight,m_mcChannelNumber);
	  yield(val_ti,"ttH1BDT",weight,m_mcChannelNumber);

          if( m_mcChannelNumber == i_tHjb ) h_te_ttHBDT1->Fill("tHjb",weight);
          if( m_mcChannelNumber == i_tWH )  h_te_ttHBDT1->Fill("tWH", weight);
          if( m_mcChannelNumber == i_VBF )  h_te_ttHBDT1->Fill("VBF", weight);
          if( m_mcChannelNumber == i_WH || m_mcChannelNumber == i_WH2 )   h_te_ttHBDT1->Fill("WH",  weight);
          if( m_mcChannelNumber == i_ZH )   h_te_ttHBDT1->Fill("ZH",  weight);
          if( m_mcChannelNumber == i_ggZH ) h_te_ttHBDT1->Fill("ggZH",weight);
          if( m_mcChannelNumber == i_bbH )  h_te_ttHBDT1->Fill("bbH", weight);

          continue;
        }
      } // End MC not used for training
      
      // Now MC used for training
      if( m_mcChannelNumber == i_ttH ||
	  m_mcChannelNumber == i_ggH ){
	weight = lumi*weight_1ifb;
	
	// ttH REGION 6                                       
	if( BDTG > B1 && BDTG <= B2 ){
	  // if testing sample
	  if( fold==1 ){
	    yield(val,"ttH6BDT",4*weight,m_mcChannelNumber);
	    yield(val_ti,"ttH6BDT",4*weight,m_mcChannelNumber);

            if( m_mcChannelNumber == i_ttH ) h_te_ttHBDT6->Fill("ttH", 4*weight);
            if( m_mcChannelNumber == i_ggH ) h_te_ttHBDT6->Fill("ggH", 4*weight);
	    continue;
	  }
	  else if( fold==2 || fold==3 ){
	    yield(val_va,"ttH6BDT",2*weight,m_mcChannelNumber);
	    yield(val_tr,"ttH6BDT",2*weight,m_mcChannelNumber);

            if( m_mcChannelNumber == i_ttH ) h_tr_ttHBDT6->Fill("ttH", 2*weight);
            if( m_mcChannelNumber == i_ggH ) h_tr_ttHBDT6->Fill("ggH", 2*weight);
	    continue;
	  }
	}
	// ttH REGION 5                                      
	if( BDTG > B2 && BDTG <= B3){
	  // if testing sample
	  if( fold==1 ){
	    yield(val,"ttH5BDT",4*weight,m_mcChannelNumber);
	    yield(val_ti,"ttH5BDT",4*weight,m_mcChannelNumber);

	    if( m_mcChannelNumber == i_ttH ) h_te_ttHBDT5->Fill("ttH", 4*weight);
	    if( m_mcChannelNumber == i_ggH ) h_te_ttHBDT5->Fill("ggH", 4*weight);
	    continue;
	  }
	  else if( fold==2 || fold==3 ){
	    yield(val_va,"ttH5BDT",2*weight,m_mcChannelNumber);
	    yield(val_tr,"ttH5BDT",2*weight,m_mcChannelNumber);

	    if( m_mcChannelNumber == i_ttH ) h_tr_ttHBDT5->Fill("ttH", 2*weight);
	    if( m_mcChannelNumber == i_ggH ) h_tr_ttHBDT5->Fill("ggH", 2*weight);
	    continue;
	  }
	}
	// ttH REGION 4                                       
	if( BDTG > B3 && BDTG <= B4 ){
	  // if testing sample
	  if( fold==1 ){
	    yield(val,"ttH4BDT",4*weight,m_mcChannelNumber);
	    yield(val_ti,"ttH4BDT",4*weight,m_mcChannelNumber);

	    if( m_mcChannelNumber == i_ttH ) h_te_ttHBDT4->Fill("ttH", 4*weight);
	    if( m_mcChannelNumber == i_ggH ) h_te_ttHBDT4->Fill("ggH", 4*weight);
	    continue;
	  }
	  else if( fold==2 || fold==3 ){
	    yield(val_va,"ttH4BDT",2*weight,m_mcChannelNumber);
	    yield(val_tr,"ttH4BDT",2*weight,m_mcChannelNumber);

	    if( m_mcChannelNumber == i_ttH ) h_tr_ttHBDT4->Fill("ttH", 2*weight);
	    if( m_mcChannelNumber == i_ggH ) h_tr_ttHBDT4->Fill("ggH", 2*weight);
	    continue;
	  }
	}
	// ttH REGION 3                                       
	if( BDTG > B4 && BDTG <= B5 ){
          // if testing sample
          if( fold==1 ){
            yield(val,"ttH3BDT",4*weight,m_mcChannelNumber);
	    yield(val_ti,"ttH3BDT",4*weight,m_mcChannelNumber);

            if( m_mcChannelNumber == i_ttH ) h_te_ttHBDT3->Fill("ttH", 4*weight);
            if( m_mcChannelNumber == i_ggH ) h_te_ttHBDT3->Fill("ggH", 4*weight);
            continue;
          }
          else if( fold==2 || fold==3 ){
	    yield(val_va,"ttH3BDT",2*weight,m_mcChannelNumber);
	    yield(val_tr,"ttH3BDT",2*weight,m_mcChannelNumber);

            if( m_mcChannelNumber == i_ttH ) h_tr_ttHBDT3->Fill("ttH", 2*weight);
            if( m_mcChannelNumber == i_ggH ) h_tr_ttHBDT3->Fill("ggH", 2*weight);
            continue;
          }
	}
        // ttH REGION 2
        if( BDTG > B5 && BDTG <= B6 ){
          // if testing sample
          if( fold==1 ){
            yield(val,"ttH2BDT",4*weight,m_mcChannelNumber);
	    yield(val_ti,"ttH2BDT",4*weight,m_mcChannelNumber);

            if( m_mcChannelNumber == i_ttH ) h_te_ttHBDT2->Fill("ttH", 4*weight);
            if( m_mcChannelNumber == i_ggH ) h_te_ttHBDT2->Fill("ggH", 4*weight);
            continue;
          }
          else if( fold==2 || fold==3 ){
	    yield(val_va,"ttH2BDT",2*weight,m_mcChannelNumber);
	    yield(val_tr,"ttH2BDT",2*weight,m_mcChannelNumber);

            if( m_mcChannelNumber == i_ttH ) h_tr_ttHBDT2->Fill("ttH", 2*weight);
            if( m_mcChannelNumber == i_ggH ) h_tr_ttHBDT2->Fill("ggH", 2*weight);
            continue;
          }
        }
	// ttH REGION 1
        if( BDTG > B6 ){
          // if testing sample
          if( fold==1 ){
            yield(val,"ttH1BDT",4*weight,m_mcChannelNumber);
            yield(val_ti,"ttH1BDT",4*weight,m_mcChannelNumber);

            if( m_mcChannelNumber == i_ttH ) h_te_ttHBDT1->Fill("ttH", 4*weight);
            if( m_mcChannelNumber == i_ggH ) h_te_ttHBDT1->Fill("ggH", 4*weight);
            continue;
          }
          else if( fold==2 || fold==3 ){
            yield(val_va,"ttH1BDT",2*weight,m_mcChannelNumber);
            yield(val_tr,"ttH1BDT",2*weight,m_mcChannelNumber);

            if( m_mcChannelNumber == i_ttH ) h_tr_ttHBDT1->Fill("ttH", 2*weight);
            if( m_mcChannelNumber == i_ggH ) h_tr_ttHBDT1->Fill("ggH", 2*weight);
            continue;
          }
        }
      } // End if MC used for training
    } // End if MC 
    
    // If data 
    if( m_mcChannelNumber <= 0 &&
	fabs(m_mgg - 125) > 5 ){
      
      if( !(flag_passedIso && flag_passedPID) ){

	double weight0 = scalefactor_not_TITI*lumi*weight_1ifb;

	// If testing sample
	if( fold==1 ){	  
	  weight = 4*weight0;

          // ttH REGION 6
          if( BDTG > B1 && BDTG <= B2 ){
            yield(val,"ttH6BDT",weight,m_mcChannelNumber);
            h_te_ttHBDT6->Fill("data",weight);
            continue;
          }
          // ttH REGION 5
          if( BDTG > B2 && BDTG <= B3){
            yield(val,"ttH5BDT",weight,m_mcChannelNumber);
            h_te_ttHBDT5->Fill("data",weight);
            continue;
          }
          // ttH REGION 4
          if( BDTG > B3 && BDTG <= B4 ){
            yield(val,"ttH4BDT",weight,m_mcChannelNumber);
            h_te_ttHBDT4->Fill("data",weight);
            continue;
          }
	  // ttH REGION 3
          if( BDTG > B4 && BDTG <= B5 ){
            yield(val,"ttH3BDT",weight,m_mcChannelNumber);
            h_te_ttHBDT3->Fill("data",weight);
            continue;
          }
          // ttH REGION 2
          if( BDTG > B5 && BDTG <= B6 ){
            yield(val,"ttH2BDT",weight,m_mcChannelNumber);
            h_te_ttHBDT2->Fill("data",weight);
            continue;
          }
          // ttH REGION 1
          if( BDTG > B6 ){
            yield(val,"ttH1BDT",weight,m_mcChannelNumber);
            h_te_ttHBDT1->Fill("data",weight);
            continue;
          }
	}
	// Is validation sample
	if( fold== 0 ){
	  weight = 4*weight0;

          // ttH REGION 6
          if( BDTG > B1 && BDTG <= B2 ){
	    yield(val_va,"ttH6BDT",weight,m_mcChannelNumber);
            h_va_ttHBDT6->Fill("data",weight);
            continue;
          }
          // ttH REGION 5
          if( BDTG > B2 && BDTG <= B3){
	    yield(val_va,"ttH5BDT",weight,m_mcChannelNumber);
            h_va_ttHBDT5->Fill("data",weight);
            continue;
          }
          // ttH REGION 4
          if( BDTG > B3 && BDTG <= B4 ){
	    yield(val_va,"ttH4BDT",weight,m_mcChannelNumber);
            h_va_ttHBDT4->Fill("data",weight);
            continue;
          }
          // ttH REGION 3
          if( BDTG > B4 && BDTG <= B5 ){
	    yield(val_va,"ttH3BDT",weight,m_mcChannelNumber);
            h_va_ttHBDT3->Fill("data",weight);
            continue;
          }
	  // ttH REGION 2
          if( BDTG > B5 && BDTG <= B6 ){
	    yield(val_va,"ttH2BDT",weight,m_mcChannelNumber);
            h_va_ttHBDT2->Fill("data",weight);
            continue;
          }
          // ttH REGION 1
          if( BDTG > B6 ){
	    yield(val_va,"ttH1BDT",weight,m_mcChannelNumber);
            h_va_ttHBDT1->Fill("data",weight);
            continue;
          }
	}
	// Is training sample
	if( fold==2 || fold==3 ){
	  weight = 2*weight0;

	  // ttH REGION 6
	  if( BDTG > B1 && BDTG <= B2 ){ 
	    yield(val_tr,"ttH6BDT",weight,m_mcChannelNumber);
	    h_tr_ttHBDT6->Fill("data",weight);
	    continue;
	  }
	  // ttH REGION 5
	  if( BDTG > B2 && BDTG <= B3){
	    yield(val_tr,"ttH5BDT",weight,m_mcChannelNumber);
	    h_tr_ttHBDT5->Fill("data",weight);
	    continue;
	  }
	  // ttH REGION 4
	  if( BDTG > B3 && BDTG <= B4 ){
	    yield(val_tr,"ttH4BDT",weight,m_mcChannelNumber);
	    h_tr_ttHBDT4->Fill("data",weight);
	    continue;
	  }
	  // ttH REGION 3
	  if( BDTG > B4 && BDTG <= B5 ){ 
	    yield(val_tr,"ttH3BDT",weight,m_mcChannelNumber);
	    h_tr_ttHBDT3->Fill("data",weight);
	    continue;
	  }
          // ttH REGION 2
          if( BDTG > B5 && BDTG <= B6 ){
	    yield(val_tr,"ttH2BDT",weight,m_mcChannelNumber);
            h_tr_ttHBDT2->Fill("data",weight);
            continue;
          }
          // ttH REGION 1
          if( BDTG > B6 ){
	    yield(val_tr,"ttH1BDT",weight,m_mcChannelNumber);
            h_tr_ttHBDT1->Fill("data",weight);
            continue;
          }
	} 
      } // End if NTNI
      
      // If TI
      if( flag_passedIso && flag_passedPID ){
	weight = scalefactor_TITI*lumi*weight_1ifb;

	// ttH REGION 6
	if( BDTG > B1 && BDTG <= B2 ){
	  yield(val_ti,"ttH6BDT",weight,m_mcChannelNumber);
	  h_ti_ttHBDT6->Fill("data",weight);
	  continue;
	}
	// ttH REGION 5
	if( BDTG > B2 && BDTG <= B3){
	  yield(val_ti,"ttH5BDT",weight,m_mcChannelNumber);
	  h_ti_ttHBDT5->Fill("data",weight);
	  continue;
	}
	// ttH REGION 4
	if( BDTG > B3 && BDTG <= B4 ){
	  yield(val_ti,"ttH4BDT",weight,m_mcChannelNumber);
	  h_ti_ttHBDT4->Fill("data",weight);
	  continue;
	}
	// ttH REGION 3
	if( BDTG > B4 && BDTG <= B5 ){
	  yield(val_ti,"ttH3BDT",weight,m_mcChannelNumber);
	  h_ti_ttHBDT3->Fill("data",weight);
	  continue;
	}
        // ttH REGION 2
        if( BDTG > B5 && BDTG <= B6 ){
	  yield(val_ti,"ttH2BDT",weight,m_mcChannelNumber);
          h_ti_ttHBDT2->Fill("data",weight);
          continue;
        }
        // ttH REGION 1
        if( BDTG > B6 ){
	  yield(val_ti,"ttH1BDT",weight,m_mcChannelNumber);
          h_ti_ttHBDT1->Fill("data",weight);
          continue;
        }
      } // End if TI
    } // End if data
    
  } // End loop over events
  
  TFile f("hist_output_2.root","recreate");
  h_tr_ttHBDT1->Write();
  h_tr_ttHBDT2->Write();
  h_tr_ttHBDT3->Write();
  h_tr_ttHBDT4->Write();
  h_tr_ttHBDT5->Write();
  h_tr_ttHBDT6->Write(); 

  h_te_ttHBDT1->Write();
  h_te_ttHBDT2->Write();
  h_te_ttHBDT3->Write();
  h_te_ttHBDT4->Write();
  h_te_ttHBDT5->Write();
  h_te_ttHBDT6->Write();

  h_ti_ttHBDT1->Write();
  h_ti_ttHBDT2->Write();
  h_ti_ttHBDT3->Write();
  h_ti_ttHBDT4->Write();
  h_ti_ttHBDT5->Write();
  h_ti_ttHBDT6->Write();

  h_va_ttHBDT1->Write();
  h_va_ttHBDT2->Write();
  h_va_ttHBDT3->Write();
  h_va_ttHBDT4->Write();
  h_va_ttHBDT5->Write();
  h_va_ttHBDT6->Write();

  f.Close();
  
  write_output(val,names,"");
  write_output(val_tr,names,"training/");
  write_output(val_va,names,"validation/");
  write_output(val_ti,names,"titi/");

  return 0 ;
}




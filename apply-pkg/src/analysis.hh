#include "TGraphAsymmErrors.h"
#include "TEfficiency.h"
#include "TLatex.h"
#include "TMath.h"
#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
#include "TCanvas.h"
#include "RooPlot.h"
#include "TROOT.h"
#include "THStack.h"
#include "TString.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH3D.h"
#include "TRandom.h"
#include "TRandom3.h"
#include "RooRealVar.h"
#include "RooExponential.h"
#include "RooPlot.h"
#include "RooGaussian.h"
#include "RooPolynomial.h"
#include "RooAddPdf.h"
#include "RooAbsData.h"
#include "RooAbsRealLValue.h"
#include "RooAbsPdf.h"
#include "RooMinuit.h"
#include "RooCategory.h"
#include "RooDataSet.h"
#include "RooArgSet.h"
#include "RooDataHist.h"
#include "RooNLLVar.h"
#include "RooSimultaneous.h"
#include "RooExponential.h"
#include "RooGlobalFunc.h"
#include "RooCBShape.h"
#include "RooFormula.h"
#include "RooRandom.h"
#include "RooFitResult.h"
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include "header.hh"
using namespace std;
using namespace RooFit;
#include <sys/stat.h>

double significance( double s , double b)
{
  if( s > 0 && b > 0 )
    return sqrt(2*((s+b)*log(1+s/b)-s));
  // see eq 97 of  https://arxiv.org/pdf/1007.1727v3.pdf
  else
    return 0;
}

double Z_interval( TH1F* h_s , TH1F* h_b, TH1F* h_bkg , TH1F* h_s2 , TH1F* h_ggH , double start = -1, double end = 1 ){

  int bin_start = h_s->FindBin(start);
  int bin_end = h_s->FindBin(end) - 1;
  double ns = h_s->Integral(bin_start,  bin_end);
  double nb = h_b->Integral(bin_start,  bin_end);
  double nb_continuum =  h_bkg->Integral(bin_start,  bin_end);
  double z =   significance( ns, nb);

  double ns_higgs = h_s2->Integral(bin_start,  bin_end);
  double n_ggH = h_ggH->Integral(bin_start,  bin_end);

  // Make sure there are enough continuum
  // background events
  if( nb_continuum <= 0.8 ){
    return 0;
  }

  if( 1.0*n_ggH/ns_higgs > .10 ){
    return 0;
  }

  return z ;

}

void yield( map<int,map<string,double> > &val, string catename, double fill_weight, int m_mcChannelNumber){
  
  int i_ttH = 345863;
  int i_ttH2 = 343436;
  int i_ttH3 = 341081;
  int i_ggH = 343981; 
  int i_ggH2 = 345306;
  int i_tWH = 341998;
  int i_tHjb = 343267;
  int i_tHjb2 = 341989;
  int i_tHjb3 = 346188;
  int i_VBF =  345041;
  int i_ZH =  345319;
  int i_WH = 345317;
  int i_WH2 = 345318;
  int i_bbH = 345315;
  int i_ggZH = 345061;

  if( m_mcChannelNumber == i_ttH || m_mcChannelNumber == i_ttH2 || m_mcChannelNumber == i_ttH3 ){
    val[0][catename] += fill_weight;
  }
  if( m_mcChannelNumber == i_tHjb || m_mcChannelNumber == i_tHjb2 || m_mcChannelNumber == i_tHjb3 ){
    val[1][catename] += fill_weight;
  }
  if( m_mcChannelNumber == i_tWH ){
    val[2][catename] += fill_weight;
  }
  if( m_mcChannelNumber == i_ggH || m_mcChannelNumber == i_ggH2 ){
    val[3][catename] += fill_weight;
  }
  if( m_mcChannelNumber == i_VBF ){
    val[4][catename] += fill_weight;
  }
  if( m_mcChannelNumber == i_WH || m_mcChannelNumber == i_WH2 ){
    val[5][catename] += fill_weight;
  }
  if( m_mcChannelNumber == i_ZH ){
    val[6][catename] += fill_weight;
  }
  if( m_mcChannelNumber == i_ggZH ){
    val[7][catename] += fill_weight;
  }
  if( m_mcChannelNumber == i_bbH ){
    val[8][catename] += fill_weight;
  }

  if( m_mcChannelNumber <= 0 ){  
    val[9][catename] += fill_weight;
  }

  return;
}

void setup_hist(TH1F* h){

  h->Sumw2();

  h->Fill("data",0);
  h->Fill("ttH", 0);
  h->Fill("tHjb",0);
  h->Fill("tWH", 0);
  h->Fill("ggH", 0);
  h->Fill("VBF", 0);
  h->Fill("WH",  0);
  h->Fill("ZH",  0);
  h->Fill("ggZH", 0);
  h->Fill("bbH", 0);
  
  return;
}

vector<double> categorization(string infile){

  TFile* f_ttH = TFile::Open(infile.c_str());
  TH1F* h_ttH  = (TH1F*)f_ttH -> Get("h_tr_BDT_ttH");
  TH1F* h_tWH  = (TH1F*)f_ttH -> Get("h_te_BDT_tWH");
  TH1F* h_tHjb = (TH1F*)f_ttH -> Get("h_te_BDT_tHjb");
  TH1F* h_ggH  = (TH1F*)f_ttH -> Get("h_tr_BDT_ggH");
  TH1F* h_VBF  = (TH1F*)f_ttH -> Get("h_te_BDT_VBF");
  TH1F* h_WH   = (TH1F*)f_ttH -> Get("h_te_BDT_WH");
  TH1F* h_ZH   = (TH1F*)f_ttH -> Get("h_te_BDT_ZH");
  TH1F* h_ggZH = (TH1F*)f_ttH -> Get("h_te_BDT_ggZH");
  TH1F* h_bbH  = (TH1F*)f_ttH -> Get("h_te_BDT_bbH");
  TH1F* h_bkg  = (TH1F*)f_ttH -> Get("h_va_BDT_bkg");

  TH1F* s_ttH = (TH1F*)h_ttH->Clone();
  TH1F* b_ttH = (TH1F*)h_tWH->Clone();
  b_ttH->Add(h_tHjb);
  b_ttH->Add(h_bkg);
  b_ttH->Add(h_ggH);
  b_ttH->Add(h_VBF);
  b_ttH->Add(h_WH);
  b_ttH->Add(h_ZH);
  b_ttH->Add(h_ggZH);
  b_ttH->Add(h_bbH);

  TH1F* s_higgs = (TH1F*)h_ttH->Clone();
  s_higgs->Add(h_tHjb);
  s_higgs->Add(h_tWH);
  s_higgs->Add(h_ggH);
  s_higgs->Add(h_VBF);
  s_higgs->Add(h_WH);
  s_higgs->Add(h_ZH);
  s_higgs->Add(h_ggZH);
  s_higgs->Add(h_bbH);

  int still_going = 0;

  int ncate = 1;
  string line;
  ifstream inputFile("inputs.txt");
  while(getline(inputFile, line)) {
    if (!line.length() || line[0] == '#')
      continue;
    istringstream iss(line);
    string a,b;
    iss>>a>>b;
    if( !a.compare("ncate") ) ncate = stoi(b);
  }
  cout << ncate << endl;

  // Brute force looping over 4-D space
  double a1=0, a2=0, a3=0, a4=0, a5=0, a6=0;
  double b1=0, b2=0, b3=0, b4=0, b5=0, b6=0;
  double zmax=0;

  // Loop over all possible boundaries
  for( int i=1000; i>0; i-- ){
    b6 = i*0.001;
    // Don't let this go forever...
    if( still_going > 20 && zmax > 0 ) break;

    still_going++;

    for( int j=i*0.1-1; j>0; j-- ){
      b5 = j*0.01;
      for( int k=j-1; k>0; k-- ){
        b4 = k*0.01;
        for( int l=k-1; l>0; l-- ){
          b3 = l*0.01;
          for( int m=l-1; m>0; m-- ){
            b2 = m*0.01;
            for( int n=m-1; n>0; n-- ){
              b1 = n*0.01;

              // Don't go through loops too many times...
              if( ncate < 2 ) j=0;
	      if( ncate < 3 ) k=0;
              if( ncate < 4 ) l=0;
              if( ncate < 5 ) m=0;
              if( ncate < 6 ) n=0;

              double z1 = Z_interval( s_ttH, b_ttH, h_bkg, s_higgs, h_ggH, b1, b2);
              double z2 = Z_interval( s_ttH, b_ttH, h_bkg, s_higgs, h_ggH, b2, b3);
              double z3 = Z_interval( s_ttH, b_ttH, h_bkg, s_higgs, h_ggH, b3, b4);
              double z4 = Z_interval( s_ttH, b_ttH, h_bkg, s_higgs, h_ggH, b4, b5);
              double z5 = Z_interval( s_ttH, b_ttH, h_bkg, s_higgs, h_ggH, b5, b6);
              double z6 = Z_interval( s_ttH, b_ttH, h_bkg, s_higgs, h_ggH, b6, 1);

              vector<double> vec_z;
              vec_z.push_back(z6);
              vec_z.push_back(z5);
	      vec_z.push_back(z4);
              vec_z.push_back(z3);
              vec_z.push_back(z2);
              vec_z.push_back(z1);

              double z = 0;
              // Loop over the categories we care about
              for(int index=0; index<ncate; index++){
                if( vec_z.at(index) > 0 ){
                  z += vec_z.at(index)*vec_z.at(index);
                }
                else{
                  z = 0;
                  break;
                }
              }
              z = sqrt(z);

              if( z > zmax){
                a1 = b1;
                a2 = b2;
                a3 = b3;
                a4 = b4;
                a5 = b5;
                a6 = b6;
		zmax = z ;
                cout << " " << a1 << " " << a2 << " " << a3 << " " << a4 << " " << a5 <<" " << a6 << " " << zmax << endl;

                still_going = 0;
              }
            } // n
          } // m
        } // l
      } // k
    } // j
  } // i

  cout << "Finished looping" << endl;
  cout << " " << a1 << " " << a2 << " " << a3 << " " << a4 << " " << a5 << " " << a6 << " " << zmax << endl;

  vector<double> vec_a;
  vec_a.push_back(a1);
  vec_a.push_back(a2);
  vec_a.push_back(a3);
  vec_a.push_back(a4);
  vec_a.push_back(a5);
  vec_a.push_back(a6);

  for(int i=0; i<6; i++){
    if(i < 6-ncate)
      vec_a.at(i) = vec_a.at(6-ncate);
  }

  ofstream output1("boundaries.txt");
  output1 << vec_a.at(0) << "\t" << vec_a.at(1) << "\t" << vec_a.at(2) << "\t" << vec_a.at(3) << "\t" << vec_a.at(4) << "\t" << vec_a.at(5) << endl;

  return vec_a;
}

void write_output(map<int,map<string,double> > &val, vector<string> names, string dir){
  
  int ncat = names.size();

  // the output streams for yield and purity tables.
  ofstream output1((dir+"yield_and_significance.txt").c_str());
  output1.precision(3);
  
  ofstream output2((dir+"purity.txt").c_str());
  output2.precision(3);
  
  ofstream output3((dir+"significance.txt").c_str());
  output3.precision(3);
  
  ofstream output4((dir+"Z_ttH.txt").c_str());
  output4.precision(3);

  // significance defined for individula categories
  double z_higgs[ncat], z_ttH[ncat] , z_tH[ncat] , z_ggH[ncat] ,  z_VBF[ncat] , z_WH[ncat] , z_ZH[ncat] ;
  // significance for combined result
  double z_higgs_comb = 0 , z_ttH_comb = 0 , z_tH_comb=0 , z_ggH_comb = 0 , z_VBF_comb = 0 , z_WH_comb = 0 , z_ZH_comb = 0;

  output1 << setw(10) << "Cate.#" ;
  output1 << setw(10) << "ttH" ;
  output1 << setw(10) << "tHjb" ;
  output1 << setw(10) << "tWH" ;
  output1 << setw(10) << "ggH" ;
  output1 << setw(10) << "VBF" ;
  output1 << setw(10) << "WH" ;
  output1 << setw(10) << "ZH" ;
  output1 << setw(10) << "ggZH" ;
  output1 << setw(10) << "bbH" ;
  output1 << setw(10) << "cont. bkg" ;
  output1 << setw(10) << "Z_higgs" ;
  output1 << setw(10) << "Z_ttH" ;
  output1 << setw(10) << "Z_tH" << endl;

  output2 << setw(10) << "Cate.#" ;
  //  output2 << setw(10) << "Higgs" ;
  output2 << setw(10) << "ttH" ;
  output2 << setw(10) << "tHjb" ;
  output2 << setw(10) << "tWH" ;
  output2 << setw(10) << "ggH" ;
  output2 << setw(10) << "VBF" ;
  output2 << setw(10) << "WH" ;
  output2 << setw(10) << "ZH" ;
  output2 << setw(10) << "ggZH" ;
  output2 << setw(10) << "bbH" ;
  output2 << setw(10) << "total Hig" ;
  output2 << setw(10) << "cont. bkg" ;
  output2 << setw(10) << "S/B Hig" ;
  output2 << setw(10) << "S/B ttH" << endl;

  output3 << setw(10) << "Cate.#" ;
  output3 << setw(10) << "Higgs" ;
  output3 << setw(10) << "ttH" ;
  output3 << setw(10) << "tH" ;
  output3 << setw(10) << "ggH" ;
  output3 << setw(10) << "VBF" ;
  output3 << setw(10) << "WH" ;
  output3 << setw(10) << "ZH" ;
  output3 << setw(10) << "ggZH" ;
  output3 << setw(10) << "bbH" << endl;

  for( int j = 0 ; j < ncat ; j ++ ){

    output1 << setw(10) << names[j];
    output2 << setw(10) << names[j];
    output3 << setw(10) << names[j];
    double sig = 0;
    
    // calculating the total Higgs signals (only loop from 0 to 8, 9 is for continuum bkg.)
    for( int i = 0 ; i < 9 ; i ++ ){
      sig +=val[i][(names[j])];
    }
    
    
    for( int i = 0 ; i < 10 ; i ++ ){
      
      // Print out the event yield for category names[j] and production model i.
      output1 << setw(10) << val[i][(names[j])]  ;
      
      // Print out the purity for category names[j] and production model i. Note that this output stream is output2 so print out is saved in a different file.
      if(i<9)
	output2 << setw(10) << 100*val[i][(names[j])]/sig  ;
      
      if( i == 9 ){
	// when we reach the last process (cont. bkg), we can calcuate the significances and s/b, because all processes yields in this category are calculated.
	z_higgs[j] = significance(val[0][(names[j])]+val[1][(names[j])]+
				  val[2][(names[j])]+val[3][(names[j])]+
				  val[4][(names[j])]+val[5][(names[j])]+
				  val[6][(names[j])]+val[7][(names[j])]+
				  val[8][(names[j])], val[9][(names[j])]);
	
	z_ttH[j] = significance(val[0][(names[j])], val[1][(names[j])]+
				val[2][(names[j])]+val[3][(names[j])]+
				val[4][(names[j])]+val[5][(names[j])]+
				val[6][(names[j])]+val[7][(names[j])]+
				val[8][(names[j])]+val[9][(names[j])]);
	
	z_tH[j] = significance(val[1][(names[j])]+val[2][(names[j])], 
			       val[0][(names[j])]+val[3][(names[j])]+
			       val[4][(names[j])]+val[5][(names[j])]+
			       val[6][(names[j])]+val[7][(names[j])]+
			       val[8][(names[j])]+val[9][(names[j])]);
	
	output1 << setw(10) << z_higgs[j]  << setw(10) << z_ttH[j] << setw(10)<< z_tH[j] << endl;
	output2 << setw(10) << sig << setw(10) << val[9][(names[j])] <<setw(10) << 100*sig/val[9][(names[j])] ;
	output2 << setw(10) << 100*val[0][(names[j])]/(sig-val[0][(names[j])] + val[9][(names[j])]) << endl ;
	output3 << setw(10) << z_higgs[j]  << setw(10) << z_ttH[j] << endl;
	//setw(10)<< z_tH[j] << setw(10)<< z_ggH[j] <<setw(10)<< z_VBF[j]<<setw(10)<< z_WH[j]<<setw(10)<< z_ZH[j]<<endl;
	
	
      }
    }

    // Quadratic sum of significance of previously processed categories and that of this current category.
    z_higgs_comb=sqrt(z_higgs_comb*z_higgs_comb + z_higgs[j]* z_higgs[j]);
    z_ttH_comb=sqrt(z_ttH_comb*z_ttH_comb + z_ttH[j]*z_ttH[j]);
    z_tH_comb=sqrt(z_tH_comb*z_tH_comb + z_tH[j]*z_tH[j]);
    
    z_ggH_comb=sqrt(z_ggH_comb*z_ggH_comb + z_ggH[j]*z_ggH[j]);
    z_VBF_comb=sqrt(z_VBF_comb*z_VBF_comb + z_VBF[j]*z_VBF[j]);
    z_WH_comb=sqrt(z_WH_comb*z_WH_comb + z_WH[j]*z_WH[j]);
    z_ZH_comb=sqrt(z_ZH_comb*z_ZH_comb + z_ZH[j]*z_ZH[j]);
    
  }
  
  output1 << setw(10) << "Combined" <<  setw(110) << z_higgs_comb << setw(10) << z_ttH_comb << setw(10) << z_tH_comb << endl;
  output3 << setw(10) << "Combined" <<  setw(10) <<z_higgs_comb  << setw(10) << z_ttH_comb << setw(10)<< z_tH_comb << setw(10)<< z_ggH_comb <<setw(10)<< z_VBF_comb<<setw(10)<< z_WH_comb<<setw(10)<< z_ZH_comb<<endl;
  output4 << z_ttH_comb << endl;

  return;
}

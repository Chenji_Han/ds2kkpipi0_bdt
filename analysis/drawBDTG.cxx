/*************************************************************************
    > File Name: DrawHist.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Sun 29 Mar 2020 03:29:22 PM CST
 ************************************************************************/

#include"bes3plotstyle.C"
//#include"./compare.cxx"

void draw_com3(TH1D* H1, string name1, TH1D* H2, string name2,TH1D* H3, string name3, string storeName){

    SetStyle();
	gStyle -> SetOptStat(0);

    Format(H1);
    Format(H2);
    Format(H3);
    TCanvas C("C","",800,600);
 
    TLegend legend(0.65,0.65,0.90,0.87);
    legend.AddEntry(H1,name1.c_str());
    legend.AddEntry(H2,name2.c_str());
    legend.AddEntry(H3,name3.c_str());
    legend.SetFillColor(0);
	
    H1->SetLineColor(kBlack);
   	H1->SetLineWidth(2);

   	H2->SetLineColor(kRed);
   	H2->SetLineWidth(2);

   	H3->SetLineColor(kBlue);
   	H3->SetLineWidth(2);

    H1 -> GetXaxis() -> SetTitle(storeName.c_str());
    H2 -> GetXaxis() -> SetTitle(storeName.c_str());
    H3 -> GetXaxis() -> SetTitle(storeName.c_str());

    H3 -> SetMaximum( H3->GetMaximum() * 1.25 );
    H3 -> GetYaxis() -> SetRangeUser(0, H3->GetMaximum()*1.5);
    H2 -> GetYaxis() -> SetRangeUser(0, H3->GetMaximum()*1.5);
    H1 -> GetYaxis() -> SetRangeUser(0, H3->GetMaximum()*1.5);

    //H1 -> GetXaxis() -> SetTitle("");

	H3 -> Draw("hist");
	H1 -> Draw("hist same");
	H2 -> Draw("hist same");
	legend.Draw("same");

    string plotName1 = string("plots/") + storeName + string(".png");
    string plotName2 = string("plots/") + storeName + string(".eps");
	C.SaveAs(plotName1.c_str());
	//C.SaveAs(plotName2.c_str());


}



void draw_com(TH1D* H1, string name1, TH1D* H2, string name2,string storeName){


    //H1 -> Scale(100/H1->Integral());
    //H2 -> Scale(100/H2->Integral());

    SetStyle();
	gStyle -> SetOptStat(0);

    Format(H1);
    Format(H2);
    TCanvas C("C","",800,600);
    //C.SetLogy();
 
    TLegend legend(0.70,0.7,0.90,0.87);
    legend.AddEntry(H1,name1.c_str());
    legend.AddEntry(H2,name2.c_str());
    legend.SetFillColor(0);
	
    H1->SetLineColor(kBlue+1);
   	H1->SetLineWidth(2);

   	H2->SetLineColor(kRed);
   	H2->SetLineWidth(2);

    H1 -> GetXaxis() -> SetTitle("BDTG");
    H2 -> GetXaxis() -> SetTitle("BDTG");

    double max = H1->GetMaximum() > H2-> GetMaximum() ? H1->GetMaximum() : H2->GetMaximum() ;
    H1 -> GetYaxis() -> SetRangeUser(0, max*1.2);
    H2 -> GetYaxis() -> SetRangeUser(0, max*1.2);

    //H1 -> GetXaxis() -> SetTitle("");
    //H2 -> GetXaxis() -> SetTitle("");

	H1 -> Draw("hist");
	H2 -> Draw("hist same");
	legend.Draw("same");

    string plotName1 = string("plots/") + storeName + string(".png");
    string plotName2 = string("plots/") + storeName + string(".eps");
	C.SaveAs(plotName1.c_str());
	C.SaveAs(plotName2.c_str());


}


void draw(TH1D* H,string titleName,string storeName){

    SetStyle();
	gStyle -> SetOptStat(0);

    Format(H);
    TCanvas C("C","",800,600);
	H -> GetXaxis() -> SetTitle(titleName.c_str());
	H -> Draw("hist");
    string plotName1 = string("plots/") + storeName + string(".png");
    string plotName2 = string("plots/") + storeName + string(".eps");
	C.SaveAs(plotName1.c_str());
	C.SaveAs(plotName2.c_str());


}

void draw2D( TH2D* H, string Xtitle, string Ytitle, string storeName ){

    SetStyle();
	gStyle -> SetOptStat(0);
	TCanvas C1;
	H -> Draw("COLZ");
    FormatAxis(H->GetYaxis());
    FormatAxis(H->GetXaxis());
    H-> GetXaxis() -> SetTitle(Xtitle.c_str());
    H-> GetYaxis() -> SetTitle(Ytitle.c_str());

    string pngName = string("./plots/") + storeName + string(".png");
    string epsName = string("./plots/") + storeName + string(".eps");
	C1.SaveAs(pngName.c_str());
	C1.SaveAs(epsName.c_str());

}

void drawBDTG() {

    string inputName1 = "../application/Ds2KKPiMCrw.root";
    TFile inputFile1(inputName1.c_str());
    cout<<"loading file "<<inputName1.c_str()<<endl;
    TTree* inputTree1 = (TTree*) inputFile1.Get("output");
    
    string inputName2 = "../application/Ds2KKPiDatarw.root";
    TFile inputFile2(inputName2.c_str());
    cout<<"loading file "<<inputName2.c_str()<<endl;
    TTree* inputTree2 = (TTree*) inputFile2.Get("output");
   
    TH1D Hsig("Hsig","",100,0.,1);
    TH1D Hbkg("Hbkg","",100,0.,1);

    inputTree1 -> Project("Hsig","BDTG","");
    inputTree2 -> Project("Hbkg","BDTG","");

    Hsig.Scale(1.0/Hsig.Integral());
    Hbkg.Scale(1.0/Hbkg.Integral());
    draw_com(&Hsig,"signal events",&Hbkg,"bkg events","sig_bkg");

}



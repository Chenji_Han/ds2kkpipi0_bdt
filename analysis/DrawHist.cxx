/*************************************************************************
    > File Name: DrawHist.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Sun 29 Mar 2020 03:29:22 PM CST
 ************************************************************************/

#include"bes3plotstyle.C"
//#include"./compare.cxx"

void draw_com3(TH1D* H1, string name1, TH1D* H2, string name2,TH1D* H3, string name3, string storeName){

    SetStyle();
	gStyle -> SetOptStat(0);

    Format(H1);
    Format(H2);
    Format(H3);
    TCanvas C("C","",800,600);
 
    TLegend legend(0.5055,0.5055,0.5050,0.87);
    legend.AddEntry(H1,name1.c_str());
    legend.AddEntry(H2,name2.c_str());
    legend.AddEntry(H3,name3.c_str());
    legend.SetFillColor(0);
	
    H1->SetLineColor(kBlack);
   	H1->SetLineWidth(2);

   	H2->SetLineColor(kRed);
   	H2->SetLineWidth(2);

   	H3->SetLineColor(kBlue);
   	H3->SetLineWidth(2);

    H1 -> GetXaxis() -> SetTitle(storeName.c_str());
    H2 -> GetXaxis() -> SetTitle(storeName.c_str());
    H3 -> GetXaxis() -> SetTitle(storeName.c_str());

    H3 -> SetMaximum( H3->GetMaximum() * 1.25 );
    H3 -> GetYaxis() -> SetRangeUser(0, H3->GetMaximum()*1.5);
    H2 -> GetYaxis() -> SetRangeUser(0, H3->GetMaximum()*1.5);
    H1 -> GetYaxis() -> SetRangeUser(0, H3->GetMaximum()*1.5);

    //H1 -> GetXaxis() -> SetTitle("");

	H3 -> Draw("hist");
	H1 -> Draw("hist same");
	H2 -> Draw("hist same");
	legend.Draw("same");

    string plotName1 = string("plots/") + storeName + string(".png");
    string plotName2 = string("plots/") + storeName + string(".eps");
	C.SaveAs(plotName1.c_str());
	//C.SaveAs(plotName2.c_str());


}



void draw_com(TH1D* H1, string name1, TH1D* H2, string name2,string storeName){


    //H1 -> Scale(100/H1->Integral());
    //H2 -> Scale(100/H2->Integral());

    SetStyle();
	gStyle -> SetOptStat(0);

    Format(H1);
    Format(H2);
    TCanvas C("C","",800,600);
    //C.SetLogy();
 
    TLegend legend(0.70,0.7,0.5050,0.87);
    legend.AddEntry(H1,name1.c_str());
    legend.AddEntry(H2,name2.c_str());
    legend.SetFillColor(0);
	
    H1->SetLineColor(kBlue+1);
   	H1->SetLineWidth(2);

   	H2->SetLineColor(kRed);
   	H2->SetLineWidth(2);

    H1 -> GetXaxis() -> SetTitle("BDTG");
    H2 -> GetXaxis() -> SetTitle("BDTG");

    H1 -> GetYaxis() -> SetRangeUser(0, H2->GetMaximum()*1.2);
    H2 -> GetYaxis() -> SetRangeUser(0, H2->GetMaximum()*1.2);

    //H1 -> GetXaxis() -> SetTitle("");
    //H2 -> GetXaxis() -> SetTitle("");

	H1 -> Draw("hist");
	H2 -> Draw("hist same");
	legend.Draw("same");

    string plotName1 = string("plots/") + storeName + string(".png");
    string plotName2 = string("plots/") + storeName + string(".eps");
	C.SaveAs(plotName1.c_str());
//	C.SaveAs(plotName2.c_str());


}


void draw(TH1D* H,string titleName,string storeName){

    SetStyle();
	gStyle -> SetOptStat(0);

    Format(H);
    TCanvas C("C","",800,600);
	H -> GetXaxis() -> SetTitle(titleName.c_str());
    H -> GetYaxis() -> SetRangeUser(0, H->GetMaximum()*1.3);
	H -> Draw("hist");
    string plotName1 = string("plots/") + storeName + string(".png");
    //string plotName2 = string("plots/") + storeName + string(".eps");
	C.SaveAs(plotName1.c_str());
	//C.SaveAs(plotName2.c_str());


}

void draw2D( TH2D* H, string Xtitle, string Ytitle, string storeName ){

    SetStyle();
	gStyle -> SetOptStat(0);
	TCanvas C1;
	H -> Draw("COLZ");
    FormatAxis(H->GetYaxis());
    FormatAxis(H->GetXaxis());
    H-> GetXaxis() -> SetTitle(Xtitle.c_str());
    H-> GetYaxis() -> SetTitle(Ytitle.c_str());

    string pngName = string("./plots/") + storeName + string(".png");
    //string epsName = string("./plots/") + storeName + string(".eps");
	C1.SaveAs(pngName.c_str());
	//C1.SaveAs(epsName.c_str());

}

void DrawHist() {

    //string inputName = "../../sample/Ds2KKPiData.root";
    //string inputName = "../sample/rootSample/Ds2KKPiDataca.root";
    string inputName = "../application/Ds2KKPiDatarw.root";
    //string inputName = "../application/Ds2KKPiDatafullcaw.root";
    TFile inputFile(inputName.c_str());
    cout<<"loading file "<<inputName.c_str()<<endl;
    TTree* inputTree = (TTree*) inputFile.Get("output");
     
    TH1D H00("H00","",60,1.9,2.03);
    //inputTree -> Project("H00","Ds_m","BDTG>0.55");
    //inputTree -> Project("H00","Ds_m","sideband==1&&BDTG>0.55");
    inputTree -> Project("H00","Ds_m","(RNDM%4==0)&&BDTG>0.55&&(-0.03<DeltaE&&DeltaE<0.03)&&Chisq1C<30&&((2.09<DsGamma_m&&DsGamma_m<2.13)||(2.09<DsRecoil_m&&DsRecoil_m<2.13))");
    draw(&H00, "M(K^{+}K^{+}#pi^{-})", "Ds_m_full_bdtCut_allcut_testSet");
   return; 
    //TH2D H2D00("H2D00","",100,1.9,2.03,100,1.85,2.58);
    //inputTree -> Project("H2D00","DsGamma_m:Ds_m","");
    //draw2D(&H2D00, "Ds_m", "DsGamma_m", "DsGamma_Dsm_sig");

    //TH2D H2D0("H2D0","",100,1.9,2.03,100,1.85,2.58);
    //inputTree -> Project("H2D0","DsRecoil_m:Ds_m","");
    //draw2D(&H2D0, "Ds_m", "DsRecoil_m", "Dsm_DsRecoil_sig");

    //TH2D H2D1("H2D1","",100,2.05,2.18,100,2.05,2.18);
    //inputTree -> Project("H2D1","DsRecoil_m:DsGamma_m","");
    //draw2D(&H2D1, "DsGamma", "DsRecoil_m", "DsGamma_DsRecoil");

    //TH2D H2D2("H2D2","",100,1.9,2.03,100,-0.05,0.05);
    //inputTree -> Project("H2D2","DeltaE:Ds_m","");
    //draw2D(&H2D2, "Ds", "#DeltaE", "DE_Ds");
 
    //TH2D H2D4("H2D4","",100,1.9,2.03,100,0,50);
    //inputTree -> Project("H2D4","Chisq1C:Ds_m","");
    //draw2D(&H2D4, "Ds", "Chisq1C", "chisq1c_Ds");
//return;       
    TH2D H2D3("H2D3","",100,1.8,2.1,100,0,1);
    inputTree -> Project("H2D3","BDTG:Ds_m","sideband==1");
    draw2D(&H2D3, "Ds", "BDTG", "bdt_Ds");
    
    TH1D H0000("H0000","",60,2.05,2.2);
    inputTree -> Project("H0000","DsGamma_m","BDTG>0.55&&sideband==1");
    draw(&H0000, "DsGamma", "DsGamma_m");
 
    TH1D H000("H000","",60,2.05,2.2);
    inputTree -> Project("H000","DsRecoil_m","BDTG>0.55&&sideband==1");
    draw(&H000, "recoil M(K^{+}K^{+}#pi^{-})", "dt_recoil");

    TH1D H13("H13","",100,-0.1,0.1);
    inputTree -> Project("H13","DeltaE","BDTG>0.55");
    draw(&H13, "#DeltaE", "deltaE");

}



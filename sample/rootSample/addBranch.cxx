/*************************************************************************
    > File Name: addBranch.cxx
    > Author: Chenji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: 2020年08月06日 星期四 12时45分30秒
 ************************************************************************/

class AddBranch{

public:

    AddBranch(TTree* tree) : _tree(tree) { _mode = 0; }
    AddBranch(string name,TTree* tree) : _name(name), _tree(tree) { _mode = 1; }
    AddBranch(string name1,string name2,TTree* tree) : _name1(name1), _name2(name2), _tree(tree) { _mode = 2; }
    void run();
    ~AddBranch(){ _tree = NULL; }

private:

    TTree* _tree;
    string _name;
    string _name1;
    string _name2;
    int _mode;

    double _eta(double theta);
    void _run_mode0();
    void _run_mode1();
    void _run_mode2();

};

double AddBranch :: _eta(double theta){

    return -1 * TMath::Log( TMath::Tan(theta/2.0) );

}

void AddBranch :: run(){
       
    switch(_mode){
        case 0 :
            _run_mode0();
            break;
        case 1 :
            _run_mode1();
            break;
        case 2 :
            _run_mode2();
            break;
    }
   
}


void AddBranch :: _run_mode2(){

    double px1, py1, pz1, e1;
    string px1_name = _name1 + string("_px");
    string py1_name = _name1 + string("_py");
    string pz1_name = _name1 + string("_pz");
    string e1_name = _name1 + string("_e");
    
    _tree -> SetBranchAddress(px1_name.c_str(), &px1);
    _tree -> SetBranchAddress(py1_name.c_str(), &py1);
    _tree -> SetBranchAddress(pz1_name.c_str(), &pz1);
    _tree -> SetBranchAddress(e1_name.c_str(), &e1);


    double px2, py2, pz2, e2;
    string px2_name = _name2 + string("_px");
    string py2_name = _name2 + string("_py");
    string pz2_name = _name2 + string("_pz");
    string e2_name = _name2 + string("_e");
    
    _tree -> SetBranchAddress(px2_name.c_str(), &px2);
    _tree -> SetBranchAddress(py2_name.c_str(), &py2);
    _tree -> SetBranchAddress(pz2_name.c_str(), &pz2);
    _tree -> SetBranchAddress(e2_name.c_str(), &e2);

    double phi, theta, eta, mass, pt;
    string phi_name   = _name1 + string("_") + _name2 + string("_Dphi");
    string phi_type   = _name1 + string("_") + _name2 + string("_Dphi/D");
    string theta_name = _name1 + string("_") + _name2 + string("_Dtheta");
    string theta_type = _name1 + string("_") + _name2 + string("_Dtheta/D");
    string eta_name = _name1 + string("_") + _name2 + string("_Deta");
    string eta_type = _name1 + string("_") + _name2 + string("_Deta/D");
    string mass_name = _name1 + string("_") + _name2 + string("_mass");
    string mass_type = _name1 + string("_") + _name2 + string("_mass/D");
    string pt_name = _name1 + string("_") + _name2 + string("_pt");
    string pt_type = _name1 + string("_") + _name2 + string("_pt/D");

    TBranch* newBranch_phi   = _tree -> Branch(phi_name.c_str(), &phi, phi_type.c_str());
    TBranch* newBranch_theta = _tree -> Branch(theta_name.c_str(), &theta, theta_type.c_str());
    TBranch* newBranch_eta = _tree -> Branch(eta_name.c_str(), &eta, eta_type.c_str());
    TBranch* newBranch_mass = _tree -> Branch(mass_name.c_str(), &mass, mass_type.c_str());
    TBranch* newBranch_pt = _tree -> Branch(pt_name.c_str(), &pt, pt_type.c_str());

    cout<<_tree->GetEntries()<<endl;
    for(int ievt=0; ievt<_tree->GetEntries(); ievt++ ){

        _tree -> GetEntry(ievt);
        if(ievt%10000==0) cout<<"process..."<<ievt<<endl;

        TLorentzVector v1(px1,py1,pz1,e1); 
        TLorentzVector v2(px2,py2,pz2,e2); 

        phi = v1.Phi() - v2.Phi();
        theta = v1.Theta() - v2.Theta();
        eta = v1.Eta() - v2.Eta();
        mass = (v1+v2).M();
        pt = (v1+v2).Pt();

        newBranch_phi -> Fill();
        newBranch_theta -> Fill();
        newBranch_eta -> Fill();
        newBranch_mass -> Fill();
        newBranch_pt -> Fill();

    }

    _tree -> ResetBranchAddresses(); 
    
}

void AddBranch :: _run_mode1(){

    double px, py, pz;
    string px_name = _name + string("_px");
    string py_name = _name + string("_py");
    string pz_name = _name + string("_pz");
    
    _tree -> SetBranchAddress(px_name.c_str(), &px);
    _tree -> SetBranchAddress(py_name.c_str(), &py);
    _tree -> SetBranchAddress(pz_name.c_str(), &pz);

    double phi, theta, eta;
    string phi_name = _name + string("_phi");
    string phi_type = _name + string("_phi/D");
    string theta_name = _name + string("_theta");
    string theta_type = _name + string("_theta/D");
    string eta_name = _name + string("_eta");
    string eta_type = _name + string("_eta/D");

    TBranch* newBranch_phi   = _tree -> Branch(phi_name.c_str(), &phi, phi_type.c_str());
    TBranch* newBranch_theta = _tree -> Branch(theta_name.c_str(), &theta, theta_type.c_str());
    TBranch* newBranch_eta = _tree -> Branch(eta_name.c_str(), &eta, eta_type.c_str());

    cout<<_tree->GetEntries()<<endl;
    for(int ievt=0; ievt<_tree->GetEntries(); ievt++ ){

        _tree -> GetEntry(ievt);
        if(ievt%10000==0) cout<<"process..."<<ievt<<endl;

        TVector3 v(px,py,pz); 

        phi = v.Phi();
        theta = v.Theta();
        eta = v.Eta();

        newBranch_phi -> Fill();
        newBranch_theta -> Fill();
        newBranch_eta -> Fill();

    }

    _tree -> ResetBranchAddresses(); 

}

void AddBranch :: _run_mode0(){


    int RNDM;
    int sideband;
    int sigregion;
    TBranch* newBranch1 = _tree -> Branch("RNDM",&RNDM,"RNDM/I");
    TBranch* newBranch2 = _tree -> Branch("sideband",&sideband,"sideband/I");
    TBranch* newBranch3 = _tree -> Branch("sigregion",&sigregion,"sigregion/I");

    double Ds_m;
    _tree -> SetBranchAddress("Ds_m",&Ds_m);

    for(int ievt=0; ievt<_tree->GetEntries(); ievt++ ){
        _tree -> GetEntry(ievt);
        if(ievt%10000==0) cout<<"process..."<<ievt<<endl;

        TRandom1 generator;
        RNDM = int(generator.Rndm()*1000);

        if(1.95<=Ds_m&&Ds_m<=1.99){
            sigregion = 1;
        }else{
            sigregion = 0;
        }

        if( (1.90<=Ds_m&&Ds_m<1.95) || (1.99<Ds_m&&Ds_m<=2.03) ){
            sideband = 1;
        }else{
            sideband = 0;
        }
        
        newBranch1 -> Fill();
        newBranch2 -> Fill();
        newBranch3 -> Fill();
            

    }

    _tree -> ResetBranchAddresses(); 

}



void _addBranch(string inputName = string("Ds2KKPiMC"),string cut = string("") ){

    string outputFileName = inputName + string("r.root");
    TFile outputFile(outputFileName.c_str(), "recreate");

    TChain inputChain("output");
    inputChain.AddFile( (inputName+string(".root")).c_str() );

    TTree* outputTree = (TTree*) inputChain.CopyTree(cut.c_str());
    
    vector<string> list {
        "K1",
        "K2",
        "Pi",
        "K1K2",
        "K1Pi",
        "K2Pi",
        "Gamma",
        "Ds",
        "DsRecoil",
        "DsGamma"
    };

    for(int i=0; i<list.size(); i++){
        cout<<"dealing with "<<list[i]<<endl;
        AddBranch instance(list[i],outputTree);
        instance.run();
    }

    for(int i=0; i<list.size()-1; i++){
        for(int j=i+1; j<list.size(); j++){
            cout<<"dealing with "<<list[i]<<", "<<list[j]<<endl;
            AddBranch instance(list[i],list[j],outputTree);
            instance.run();
        }
    }

    AddBranch instance_(outputTree);
    instance_.run();
    
    outputFile.cd();
    outputTree -> Write();
    outputFile.Close();

}



void addBranch(){

    _addBranch("Ds2KKPiMC","");
    _addBranch("Ds2KKPiData","");
    //_addBranch("Ds2KKPiDatafullc","");

}






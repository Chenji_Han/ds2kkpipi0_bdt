#!/usr/bin/env python
from __future__ import print_function
import numpy as np
import pandas as pd
import ROOT
import time


class root2csv:
    def __init__(self,in_file_name,out_file_name,tree_name='output',tag=0):
        self.in_file = ROOT.TFile.Open(in_file_name)
        self.out_csv = out_file_name + ".csv"
        self.out_h5 = out_file_name + ".h5"
        self.tree = self.in_file.Get(tree_name)
        self.tag = tag
        self.n_entries = self.tree.GetEntries()
        print('Skimmed events: %i'%self.n_entries)

    def loop(self):

        t0 = time.time()

        print('Creating csv file %s',self.out_csv)
        print('Creating h5 file %s',self.out_h5)
        print('Number of entries: %i'%self.n_entries)

        feature_list = []
        with open("inputs.txt") as f:
            for line in f:
                if line[0] == "#":
                    continue
                if line.strip():
                    (key, val) = line.split()
                    if( key == 'train_vbl' ):
                        feature_list.append(val)

        feature_list.append('RNDM')
        feature_list.append('Ds_m')
        feature_list.append('sideband')
        feature_list.append('sigregion')

#        feature_list = ['K1Pi_m','K2Pi_m','PiPi0_m','Ds_p','Chisq1C','DeltaE','DsGamma_m','DsGamma_pt','DsRecoil_m',
#                        'DsRecoil_pt','K1K2_m','RNDM']
        tally = 0
        array = []

        print("Included variables:")
        print(feature_list)

        vbl_list = feature_list[:]
        header = vbl_list

        print("Runtime = " + str(time.time()-t0) + " seconds")

        for entry in range(self.n_entries):
            if entry % 1e5 == 0:
                print ('processing entry %i' % entry)
            self.tree.GetEntry(entry)
            
            if self.tag == 1:
                if  getattr(self.tree,"sigregion")==1  :
                    array += [[getattr(self.tree,feature) for feature in feature_list]]
                    tally = tally + 1
            elif self.tag == 0 :
                if  getattr(self.tree,"sideband")==1  :
                    array += [[getattr(self.tree,feature) for feature in feature_list]]
                    tally = tally + 1

        print("Runtime = " + str(time.time()-t0) + " seconds")
        
        df = pd.DataFrame(array,columns=header)

        df = df.astype('float32')
        df.index.name = "index"

        print("Runtime = " + str(time.time()-t0) + " seconds")
        print("Saving csv file")
        df.to_csv(self.out_csv,sep=' ')

        print("Runtime = " + str(time.time()-t0) + " seconds")
        print("Saving h5 file")
        store = pd.HDFStore(self.out_h5, complevel=9, complib='blosc:blosclz')
        store['d1'] = df
        store.close()

        print(str(tally) + " events pass pre-selection")
        print("Runtime = " + str(time.time()-t0) + " seconds")
        print("Done")


def main():

    import root2csv

    sig = root2csv.root2csv('rootSample/Ds2KKPiMCr.root','Ds2KKPiMCr',tag=1)
    sig.loop()

    bkg = root2csv.root2csv('rootSample/Ds2KKPiDatar.root','Ds2KKPiDatar',tag=0)
    bkg.loop()

if __name__ == "__main__":
    main()

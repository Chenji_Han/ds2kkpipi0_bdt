
#%%
# Jennet makes some correlation plots
# April 12, 2019

import matplotlib
matplotlib.use('Agg')

import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

vbls = []
with open("../inputs.txt") as f:
    for line in f:
        if line[0] == "#":
            continue
        if line.strip():
            (key, val) = line.split()
            if( key == 'train_vbl' ):
                vbls.append(val)

vbls.append('Ds_m')

print("the input training variables are {0}".format(vbls))
        
print("start loading")
#%%
df_sig = pd.read_hdf('../sample/Ds2KKPiMCr.h5','d1')
df_sig = df_sig[vbls]
df_sig = df_sig[:500]

df_bkg = pd.read_hdf('../sample/Ds2KKPiDatar.h5','d1')
df_bkg = df_bkg[vbls]
df_bkg = df_bkg[:500]
print("finish loading")

#%%
#df_ttH.head()
#
#df_data.head()
#
#
##%%
## Basic correlogram
#fig1 = sns.pairplot(df_sig,markers="o",plot_kws=dict(s=5, color='#1f77b4',edgecolor='#1f77b4', linewidth=1))
##plt.show()
#fig1.savefig("plots/sig_pairplot.png")
#
#fig3 = sns.pairplot(df_bkg,markers="o",plot_kws=dict(s=5, color='#1f77b4',edgecolor='#1f77b4', linewidth=1))
##plt.show()
#fig3.savefig("plots/bkg_pairplot.png")



#%%
# Linear correlation coefficient
def plot_corr(df,title,savename,size=12):
    corr = df.corr()
    fig, ax = plt.subplots(figsize=(size, size))
    print("start")    
    for i in range(0,len(corr.columns)) :
        for j in range(0,len(corr.columns)) :
            c = 100.*corr[corr.columns[i]][corr.columns[j]]
            if c < 101:
                ax.text(i, j, str(int(c)), va='center', ha='center')
    
    matshowobj = ax.matshow(100.*corr,cmap=plt.cm.bwr)
    plt.xticks(range(len(corr.columns)), corr.columns, rotation=90);
    plt.yticks(range(len(corr.columns)), corr.columns);
    plt.title(title,pad=-715)
    matshowobj.set_clim(-100,100)
    plt.savefig(savename)


#%%
print("Ds2KKPiMCa.h5")
plot_corr(df_sig,"signal events","./plots/sig_corr.png")

print("Ds2KKPiDataa.h5")
plot_corr(df_bkg,"side band events","./plots/bkg_corr.png")




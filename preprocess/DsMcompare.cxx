/*************************************************************************
    > File Name: compare.cxx
    > Author: Chen-Ji Han
    > Mail: hanchenji16@mails.ucas.ac.cn 
    > Created Time: Mon 13 Jan 2020 09:38:33 AM CST
 ************************************************************************/

#include"../analysis/DrawHist.cxx"

void compare_sub(TH1D* Hreco,TH1D* Htruth,string titleName,int binNum,double axisMin,double axisMax,string storeName){

     SetStyle();

    TCanvas* c=new TCanvas("c","",800,600);
    SetStyle();

    TPad *pad1 = new TPad("pad1", "pad1", 0, 0.35, 1, 1.0);
    pad1->SetBottomMargin(0); // Upper and lower plot are joined
    pad1->SetGridx();         // Vertical grid
    pad1->Draw();             // Draw the upper pad: pad1
    
    pad1->cd();               // pad1 becomes the current pad
    //SetStyle();
    Hreco->SetStats(0);          // No statistics on upper plot
    Htruth->SetStats(0);          // No statistics on upper plot
    // gPad->SetOptTitle(0);
    //gPad->SetTitle("");
    Htruth->SetMarkerSize(0);
    Hreco->SetMarkerSize(0);
     
    Htruth->Scale(1.0/Htruth->Integral());
    Hreco->Scale(1.0/Hreco->Integral());

    TLegend* legend = new TLegend(0.60,0.67,0.90,0.87);
    legend -> AddEntry(Hreco,"signal");
    legend -> AddEntry(Htruth,"bkg");
   
    legend->SetFillColor(0);
    
    FormatAxis(Hreco->GetYaxis());
    FormatAxis(Hreco->GetXaxis());
    Hreco->GetYaxis()->SetTitleOffset(0.8);
 
    Hreco->GetYaxis()->SetTitle("Events");
   	
    Hreco->GetXaxis()->SetTitle("");
   	// Hreco->GetXaxis()->SetTitleOffset();
   	Hreco->GetXaxis()->SetTitleSize(0.08);
    Hreco->SetMaximum(Hreco->GetMaximum()*1.2);
    Hreco->GetYaxis()->SetRangeUser(0.001, Hreco->GetMaximum()*1.25);
    //hsig->SetMinimum(1);
    ////gPad->SetLogy();
    TGaxis* xaxis = (TGaxis*)Hreco->GetYaxis();
    xaxis->SetMaxDigits(3);

    Hreco->SetLineColor(kBlue+1);
   	Hreco->SetLineWidth(2);

   	// hsig2 settings
   	Htruth->SetLineColor(kRed);
   	Htruth->SetLineWidth(2);
    Htruth->Scale(Hreco->Integral()/Htruth->Integral());
    Htruth->Scale(Hreco->Integral()/Htruth->Integral());

    Hreco->Draw("HIST");
    Htruth->Draw("same HIST");
    legend -> Draw("same");
    
    pad1->Update();

	// lower plot will be in pad
   	c->cd();          // Go back to the main canvas before defining pad2
    SetStyle();
   	TPad *pad2 = new TPad("pad2", "pad2", 0, 0.0, 1, 0.35);
   	pad2->SetTopMargin(0.001);
   	pad2->SetBottomMargin(0.4);
   	pad2->SetGridx(); // vertical grid
   	pad2->Draw();
   	pad2->cd();       // pad2 becomes the current pad

	// Define the ratio plot
    Double_t binerr;
    TH1F* h3 = new TH1F("h3","h3",binNum,axisMin,axisMax);
    TH1F* h4 = new TH1F("h4","h4",binNum,axisMin,axisMax);
    h3->Add(Hreco,Htruth,1,-1);
    for (int i=1; i<=binNum; i++){
        binerr = Hreco->GetBinError(i);
        if (binerr!=0){
            h4->SetBinContent(i, binerr);
        }
    }
    h3->Divide(h3,h4);
    for (int j=1;j<binNum+1;j++){
        h3->SetBinError(j, 1.0);
    }
   	//TH1F *h3 = (TH1F*)hsig->Clone("h3");
   	h3->SetLineColor(kBlack);
   	h3->SetMinimum(-1);  // Define Y ..
   	h3->SetMaximum(1); // .. range
   	
    h3->SetMarkerStyle(21);

   	// hsig settings
   	// Ratio plot (h3) settings
   	h3->GetXaxis()->SetTitle(titleName.c_str()); 


   	// Y axis ratio plot settings
   	h3->GetYaxis()->SetTitle("#chi");
    h3->SetMarkerSize(0.5);
    FormatAxis(h3->GetYaxis());
    FormatAxis(h3->GetXaxis());
   	h3->GetYaxis()->SetTitleSize(0.13);
   	h3->GetYaxis()->SetLabelSize(0.13); 
   	h3->GetXaxis()->SetLabelSize(0.13); 
    h3->GetXaxis()->SetTitleSize(0.15);
   	h3->GetYaxis()->SetTitleOffset(0.3);
   	h3->GetXaxis()->SetTitleOffset(1.24);
   
   	h3->Draw("E1");       // Draw the ratio plot

    //hsig->Draw("E");
    //hsig2->Draw("same:E");
    //TLegend *leg = new TLegend(0.58,0.7,0.83,0.8 );
    //leg->AddEntry(hsig,"Data");
    //Format(leg);
    //leg->Draw();
    // if you want draw an arrow , add the following statements
    //TArrow *arr = new TArrow(0,1000,0,0,0.01,">");
    //Format(arr);
    //arr->Draw(); 
    
    c->SaveAs((string("./plots/")+storeName+string(".png")).c_str());
    c->SaveAs((string("./plots/")+storeName+string(".eps")).c_str());

    delete h3;
    delete h4;
    delete c;

    return;
}

bool hasEnding(string target, string ending){

    if (target.length() >= ending.length()) {
        return (0 == target.compare (target.length() - ending.length(), ending.length(), ending));
    } else {
        return false;
    }

}

double setXMin(string input){

    if( hasEnding(input, "_phi" ) ){
        return -3.15; 
    }else if( hasEnding(input, "_theta") ){
        return 0; 
    }else if( hasEnding(input, "_eta") ){
        return -5;
    }else if( hasEnding(input, "_Dphi") ){
        return -6.3;
    }else if( hasEnding(input, "_Dtheta") ){
        return -3.15;
    }else if( hasEnding(input, "_Deta") ){
        return -10;
    }else{
        return 0;
    }

}


double setXMax(string input){

    if( hasEnding(input, "_phi" ) ){
        return 3.15; 
    }else if( hasEnding(input, "_theta") ){
        return 3.15; 
    }else if( hasEnding(input, "_eta") ){
        return 5;
    }else if( hasEnding(input, "_Dphi") ){
        return 6.3;
    }else if( hasEnding(input, "_Dtheta") ){
        return 3.15;
    }else if( hasEnding(input, "_Deta") ){
        return 10;
    }else{
        return 4;
    }

}

void DsMcompare(){
        
    SetStyle();
    gStyle->SetPadRightMargin(0.15);

    TFile* inputFile = new TFile("../sample/rootSample/Ds2KKPiDatar.root");
    TTree *inputTree = (TTree*) inputFile->Get("output");

    TH2D H2D00("H2D00","",100,1.9,2.03,100,2,2.2);
    inputTree -> Project("H2D00","DsGamma_m:Ds_m","sideband==1&&2.055<DsRecoil_m&&DsRecoil_m<2.15&&DsGamma_m>2.05&&DsGamma_m<2.22");
    draw2D(&H2D00, "Ds_m", "DsGamma_m", "DsGamma_Dsm_sig");

    TH2D H2D0("H2D0","",100,1.9,2.03,100,2,2.2);
    inputTree -> Project("H2D0","DsRecoil_m:Ds_m","sideband==1&&2.055<DsRecoil_m&&DsRecoil_m<2.15&&DsGamma_m>2.05&&DsGamma_m<2.22");
    draw2D(&H2D0, "Ds_m", "DsRecoil_m", "Dsm_DsRecoil_sig");

    TH2D H2D1("H2D1","",100,1.9,2.03,100,0,1.);
    inputTree -> Project("H2D1","DsRecoil_pt:Ds_m","sideband==1&&2.055<DsRecoil_m&&DsRecoil_m<2.15&&DsGamma_m>2.05&&DsGamma_m<2.22");
    draw2D(&H2D1, "Ds_m", "DsRecoil_pt", "Dsm_DsRecoil_pt_sig");


}



